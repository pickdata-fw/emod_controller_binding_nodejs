#include "DInput2Relay1Temp2Napi.hpp"

#include <napi.h>

#include <cstring>
#include <string>

#include "Controller.hpp"

Napi::FunctionReference DInput2Relay1Temp2Napi::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void DInput2Relay1Temp2Napi_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	// calls the javascript function that have been passed to the DInput2Relay1Temp2Napi constructor
	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void DInput2Relay1Temp2Napi_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	DInput2Relay1Temp2Napi* module = (DInput2Relay1Temp2Napi*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	// Call to the client node javascript function must be by means of a NapiThreadSafe BlockingCall
	module->callback.BlockingCall(params, DInput2Relay1Temp2Napi_mainthread_callback);
}
Napi::Object DInput2Relay1Temp2Napi::Init(Napi::Env env) {
	Napi::Function func = DefineClass(
		env, "DInput2Relay1Temp2Napi",
		{
			// constants
			StaticValue("kMaxTemperature", Napi::Number::New(env, DInput2Relay1Temp2Module::kMaxTemperature)),
			StaticValue("kMinTemperature", Napi::Number::New(env, DInput2Relay1Temp2Module::kMinTemperature)),
			StaticValue("kUnplugged", Napi::Number::New(env, DInput2Relay1Temp2Module::kUnplugged)),

			StaticValue("kNumberOfDigitalInputs", Napi::Number::New(env, DInput2Relay1Temp2Module::kNumberOfDigitalInputs)),
			StaticValue("kNumberOfRelays", Napi::Number::New(env, DInput2Relay1Temp2Module::kNumberOfRelays)),
			StaticValue("kNumberOfTemperatureSensors", Napi::Number::New(env, DInput2Relay1Temp2Module::kNumberOfTemperatureSensors)),

			StaticValue("kDigitalInputAll", Napi::Number::New(env, DInput2Relay1Temp2Module::kDigitalInputAll)),
			StaticValue("kRelayAll", Napi::Number::New(env, DInput2Relay1Temp2Module::kRelayAll)),
			StaticValue("kTempSensorAll", Napi::Number::New(env, DInput2Relay1Temp2Module::kTempSensorAll)),

			StaticValue("kPretriggerTime", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::DigitalInputFunctionMode::kPretriggerTime))),
			StaticValue("kPulseCounter", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::DigitalInputFunctionMode::kPulseCounter))),
			StaticValue("kWidthCounter", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::DigitalInputFunctionMode::kWidthCounter))),
			StaticValue("kTwoWirePt100Sensor", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kTwoWirePt100Sensor))),
			StaticValue("kThreeWirePt100Sensor",
						Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kThreeWirePt100Sensor))),
			StaticValue("kFourWirePt100Sensor", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kFourWirePt100Sensor))),
			StaticValue("kTwoWirePt1000Sensor", Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kTwoWirePt1000Sensor))),
			StaticValue("kThreeWirePt1000Sensor",
						Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kThreeWirePt1000Sensor))),
			StaticValue("kFourWirePt1000Sensor",
						Napi::Number::New(env, static_cast<double>(DInput2Relay1Temp2Module::TempSensorConfig::kFourWirePt1000Sensor))),

			// auxiliar functions
			StaticMethod("checkModulePresent", &DInput2Relay1Temp2Napi::checkModulePresent),

			InstanceMethod("getFunctionsOffsetId", &DInput2Relay1Temp2Napi::getFunctionsOffsetId),
			InstanceMethod("getInputsOffsetId", &DInput2Relay1Temp2Napi::getInputsOffsetId),

			// digital input functions
			InstanceMethod("getStatus", &DInput2Relay1Temp2Napi::getStatus),
			InstanceMethod("getAllStatus", &DInput2Relay1Temp2Napi::getAllStatus),
			InstanceMethod("setPulseFilterTime", &DInput2Relay1Temp2Napi::setPulseFilterTime),
			InstanceMethod("setAllPulseFilterTime", &DInput2Relay1Temp2Napi::setAllPulseFilterTime),
			InstanceMethod("getPulseFilterTime", &DInput2Relay1Temp2Napi::getPulseFilterTime),
			InstanceMethod("getAllPulseFilterTime", &DInput2Relay1Temp2Napi::getAllPulseFilterTime),
			InstanceMethod("getPulseCount", &DInput2Relay1Temp2Napi::getPulseCount),
			InstanceMethod("getAllPulseCount", &DInput2Relay1Temp2Napi::getAllPulseCount),
			InstanceMethod("resetPulseCount", &DInput2Relay1Temp2Napi::resetPulseCount),
			InstanceMethod("resetAllPulseCount", &DInput2Relay1Temp2Napi::resetAllPulseCount),
			InstanceMethod("getPulseWidth", &DInput2Relay1Temp2Napi::getPulseWidth),
			InstanceMethod("getAllPulseWidth", &DInput2Relay1Temp2Napi::getAllPulseWidth),

			// relay functions
			InstanceMethod("configPulseWidth", &DInput2Relay1Temp2Napi::configPulseWidth),
			InstanceMethod("configAllPulseWidth", &DInput2Relay1Temp2Napi::configAllPulseWidth),
			InstanceMethod("activate", &DInput2Relay1Temp2Napi::activate),
			InstanceMethod("deactivate", &DInput2Relay1Temp2Napi::deactivate),
			InstanceMethod("activateAll", &DInput2Relay1Temp2Napi::activateAll),
			InstanceMethod("deactivateAll", &DInput2Relay1Temp2Napi::deactivateAll),
			InstanceMethod("getRelayStatus", &DInput2Relay1Temp2Napi::getRelayStatus),
			InstanceMethod("getAllRelayStatus", &DInput2Relay1Temp2Napi::getAllRelayStatus),

			// temperature sensor functions
			InstanceMethod("configTempSensor", &DInput2Relay1Temp2Napi::configTempSensor),
			InstanceMethod("configAllTempSensor", &DInput2Relay1Temp2Napi::configAllTempSensor),
			InstanceMethod("getTempSensorConfig", &DInput2Relay1Temp2Napi::getTempSensorConfig),
			InstanceMethod("getTempSensor", &DInput2Relay1Temp2Napi::getTempSensor),
			InstanceMethod("getAllTempSensor", &DInput2Relay1Temp2Napi::getAllTempSensor),

			// DI callbacks
			InstanceMethod("configEventAtTimeIntervalDI", &DInput2Relay1Temp2Napi::configEventAtTimeIntervalDI),
			InstanceMethod("switchToModeDI", &DInput2Relay1Temp2Napi::switchToModeDI),
			InstanceMethod("configEventOnNewDataDI", &DInput2Relay1Temp2Napi::configEventOnNewDataDI),
			InstanceMethod("configEventOnValueChangeDI", &DInput2Relay1Temp2Napi::configEventOnValueChangeDI),

			// Temp callbacks
			InstanceMethod("configEventAtTimeIntervalTemp", &DInput2Relay1Temp2Napi::configEventAtTimeIntervalTemp),
			InstanceMethod("configEventOnValueChangeTemp", &DInput2Relay1Temp2Napi::configEventOnValueChangeTemp),
			InstanceMethod("configEventWithinRangeTemp", &DInput2Relay1Temp2Napi::configEventWithinRangeTemp),
			InstanceMethod("configEventOutOfRangeTemp", &DInput2Relay1Temp2Napi::configEventOutOfRangeTemp),

			InstanceMethod("resetEventConfig", &DInput2Relay1Temp2Napi::resetEventConfig),
		});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

DInput2Relay1Temp2Napi::DInput2Relay1Temp2Napi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<DInput2Relay1Temp2Napi>(info) {
	Napi::Env env = info.Env();
	DInput2Relay1Temp2Module::DInput2Relay1Temp2ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "DInput2Relay1Temp2Napi_callback", 0, 1);
		callback_func = DInput2Relay1Temp2Napi_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new DInput2Relay1Temp2Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 2DI+1PR+2T module").ThrowAsJavaScriptException();
		return;
	}
}

DInput2Relay1Temp2Napi::~DInput2Relay1Temp2Napi() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value DInput2Relay1Temp2Napi::checkModulePresent(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeDI2PR1T2, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

Napi::Value DInput2Relay1Temp2Napi::getInputsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getInputFunctionsOffsetIdDI();
	return Napi::Number::New(env, offset_id);
}

Napi::Value DInput2Relay1Temp2Napi::getFunctionsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getFunctionsOffsetIdDI();
	return Napi::Number::New(env, offset_id);
}

Napi::Value DInput2Relay1Temp2Napi::getStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	uint8_t status;

	if (this->module->getStatus(input_number.Uint32Value(), &status) != 0) {
		Napi::TypeError::New(env, "Error getting digital input status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, status != 0);
}

Napi::Value DInput2Relay1Temp2Napi::getAllStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t status[DInput2Relay1Temp2Module::kNumberOfDigitalInputs];

	if (this->module->getAllStatus(status) != 0) {
		Napi::TypeError::New(env, "Error getting all digital inputs status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfDigitalInputs);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfDigitalInputs; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] != 0);
	}

	return status_array;
}

void DInput2Relay1Temp2Napi::setPulseFilterTime(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "ms_time parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_time = info[1].As<Napi::Number>();

	if (this->module->setPulseFilterTime(input_number.Uint32Value(), ms_time.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting digital input pulse filter time").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::setAllPulseFilterTime(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "ms_time parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_time = info[0].As<Napi::Number>();

	if (this->module->setAllPulseFilterTime(ms_time.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting all digital inputs pulse filter time").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput2Relay1Temp2Napi::getPulseFilterTime(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	uint32_t ms_time;

	if (this->module->getPulseFilterTime(input_number.Uint32Value(), &ms_time) != 0) {
		Napi::TypeError::New(env, "Error getting pulse filter time").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, ms_time);
}

Napi::Value DInput2Relay1Temp2Napi::getAllPulseFilterTime(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t status[DInput2Relay1Temp2Module::kNumberOfDigitalInputs];

	if (this->module->getAllPulseFilterTime(status) != 0) {
		Napi::TypeError::New(env, "Error getting all pulse filter time").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfDigitalInputs);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfDigitalInputs; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] != 0);
	}

	return status_array;
}

Napi::Value DInput2Relay1Temp2Napi::getPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t count;

	if (this->module->getPulseCount(input_number.Uint32Value(), &count) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, count);
}

Napi::Value DInput2Relay1Temp2Napi::getAllPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t counts[DInput2Relay1Temp2Module::kNumberOfDigitalInputs];

	if (this->module->getAllPulseCount(counts) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array counts_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfDigitalInputs);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfDigitalInputs; i++) {
		counts_array[i] = Napi::Number::New(env, counts[i]);
	}

	return counts_array;
}

void DInput2Relay1Temp2Napi::resetPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (this->module->resetPulseCount(input_number.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::resetAllPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->resetAllPulseCount() != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput2Relay1Temp2Napi::getPulseWidth(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t width;

	if (this->module->getPulseWidth(input_number.Uint32Value(), &width) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse width").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, width);
}

Napi::Value DInput2Relay1Temp2Napi::getAllPulseWidth(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t widths[DInput2Relay1Temp2Module::kNumberOfDigitalInputs];

	if (this->module->getAllPulseWidth(widths) != 0) {
		Napi::TypeError::New(env, "Error getting all pulse widths").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array widths_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfDigitalInputs);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfDigitalInputs; i++) {
		widths_array[i] = Napi::Boolean::New(env, widths[i] != 0);
	}

	return widths_array;
}

void DInput2Relay1Temp2Napi::configPulseWidth(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return;
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput2Relay1Temp2Module::kNumberOfRelays) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of " + std::to_string(DInput2Relay1Temp2Module::kNumberOfRelays) + " element(s)")
			.ThrowAsJavaScriptException();
		return;
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "width_ms parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number width_ms = info[1].As<Napi::Number>();

	if (this->module->configPulseWidth(relay_mask, width_ms.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting pulse width").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configAllPulseWidth(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "width_ms parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number width_ms = info[0].As<Napi::Number>();

	if (this->module->configAllPulseWidth(width_ms.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting all pulse width").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput2Relay1Temp2Napi::activate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput2Relay1Temp2Module::kNumberOfRelays) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of " + std::to_string(DInput2Relay1Temp2Module::kNumberOfRelays) + " element(s)")
			.ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->activate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfRelays);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfRelays; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput2Relay1Temp2Napi::deactivate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput2Relay1Temp2Module::kNumberOfRelays) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of " + std::to_string(DInput2Relay1Temp2Module::kNumberOfRelays) + " element(s)")
			.ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->deactivate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfRelays);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfRelays; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput2Relay1Temp2Napi::activateAll(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;
	if (this->module->activateAll() != 0) {
		Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfRelays);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfRelays; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput2Relay1Temp2Napi::deactivateAll(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->deactivateAll() != 0) {
		Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfRelays);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfRelays; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput2Relay1Temp2Napi::getRelayStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput2Relay1Temp2Module::kNumberOfRelays) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of " + std::to_string(DInput2Relay1Temp2Module::kNumberOfRelays) + " element(s)")
			.ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	uint8_t status = 0;

	if (this->module->getRelayStatus(relay_mask, &status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env);

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

Napi::Value DInput2Relay1Temp2Napi::getAllRelayStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->getAllRelayStatus(&status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfRelays);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfRelays; i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

void DInput2Relay1Temp2Napi::configTempSensor(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor_id must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor_id = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "config must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number config = info[1].As<Napi::Number>();

	if (this->module->configTempSensor(static_cast<DInput2Relay1Temp2Module::TempSensorID>(sensor_id.Uint32Value()),
									   static_cast<DInput2Relay1Temp2Module::TempSensorConfig>(config.Uint32Value())) != 0) {
		Napi::TypeError::New(env, "Error setting temperature sensor configuration").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configAllTempSensor(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor_id must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number config = info[0].As<Napi::Number>();

	if (this->module->configAllTempSensor(static_cast<DInput2Relay1Temp2Module::TempSensorConfig>(config.Uint32Value())) != 0) {
		Napi::TypeError::New(env, "Error setting all temperature sensors Configuration").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput2Relay1Temp2Napi::getTempSensorConfig(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor_id must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor_id = info[0].As<Napi::Number>();

	DInput2Relay1Temp2Module::TempSensorConfig config;

	if (this->module->getTempSensorConfig(static_cast<DInput2Relay1Temp2Module::TempSensorID>(sensor_id.Uint32Value()), &config) != 0) {
		Napi::TypeError::New(env, "Error getting temperature sensor configuration").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, static_cast<uint16_t>(config));
}

Napi::Value DInput2Relay1Temp2Napi::getTempSensor(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor_id must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor_id = info[0].As<Napi::Number>();

	int16_t value;

	if (this->module->getTempSensor(static_cast<DInput2Relay1Temp2Module::TempSensorID>(sensor_id.Uint32Value()), &value) != 0) {
		Napi::TypeError::New(env, "Error getting temperature sensor, check configuration").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, value);
}

Napi::Value DInput2Relay1Temp2Napi::getAllTempSensor(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	std::vector<int16_t> data;

	if (this->module->getAllTempSensor(&data) != 0) {
		Napi::TypeError::New(env, "Error getting temperature sensor, check configuration").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array value_array = Napi::Array::New(env, DInput2Relay1Temp2Module::kNumberOfTemperatureSensors);

	for (uint32_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfTemperatureSensors; i++) {
		value_array[i] = Napi::Number::New(env, data[i]);
	}

	return value_array;
}

void DInput2Relay1Temp2Napi::configEventAtTimeIntervalDI(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_out = info[0].As<Napi::Number>();
	if (this->module->configEventAtTimeIntervalDI(time_out.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::switchToModeDI(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "mode parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number mode = info[1].As<Napi::Number>();

	if (this->module->switchToModeDI(input_number.Uint32Value(), static_cast<DInput2Relay1Temp2Module::DigitalInputFunctionMode>(mode.Uint32Value())) != 0) {
		Napi::TypeError::New(env, "Error switching mode").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventOnNewDataDI(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->configEventOnNewDataDI() != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventOnValueChangeDI(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one parameter").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput2Relay1Temp2Module::kDigitalInputAll);
	}

	if (this->module->configEventOnValueChangeDI(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventAtTimeIntervalTemp(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_interval parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_interval = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput2Relay1Temp2Module::kTempSensorAll);
	}

	if (this->module->configEventAtTimeIntervalTemp(time_interval.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventOnValueChangeTemp(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "threshold parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput2Relay1Temp2Module::kTempSensorAll);
	}

	if (this->module->configEventOnValueChangeTemp(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventWithinRangeTemp(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput2Relay1Temp2Module::kTempSensorAll);
	}

	if (this->module->configEventWithinRangeTemp(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::configEventOutOfRangeTemp(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput2Relay1Temp2Module::kTempSensorAll);
	}

	if (this->module->configEventOutOfRangeTemp(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput2Relay1Temp2Napi::resetEventConfig(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	// EDU: why this variable?
	Napi::Boolean update_module;

	if (info.Length() > 0) {
		update_module = info[0].As<Napi::Boolean>();
	} else {
		update_module = Napi::Boolean::New(env, true);
	}

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "Error resetting event config").ThrowAsJavaScriptException();
		return;
	}
}