#ifndef RELAY_MODULE_H
#define RELAY_MODULE_H

#include <napi.h>

#include "Relay8Module.hpp"

class relay_module : public Napi::ObjectWrap<relay_module> {
public:
	static Napi::Function Init(Napi::Env env);
	relay_module(const Napi::CallbackInfo& info);
	~relay_module();

private:
	static Napi::Value check_module_present(const Napi::CallbackInfo& info);

	void config_pulse_width(const Napi::CallbackInfo& info);
	Napi::Value activate(const Napi::CallbackInfo& info);
	Napi::Value deactivate(const Napi::CallbackInfo& info);
	Napi::Value activate_all(const Napi::CallbackInfo& info);
	Napi::Value deactivate_all(const Napi::CallbackInfo& info);
	Napi::Value setStatus(const Napi::CallbackInfo& info);
	Napi::Value get_relay_status(const Napi::CallbackInfo& info);
	Napi::Value get_all_relay_status(const Napi::CallbackInfo& info);

	Relay8Module* module;
	static Napi::FunctionReference constructor;
};

#endif