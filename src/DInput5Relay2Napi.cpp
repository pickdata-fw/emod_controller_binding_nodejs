#include "DInput5Relay2Napi.hpp"

#include <cstring>

#include "Controller.hpp"
#include "HAL.hpp"

Napi::FunctionReference DInput5Relay2Napi::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void DInput5Relay2Napi_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	// calls the javascript function that have been passed to the DInput5Relay2Napi constructor
	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) {
		if (params->data != nullptr) delete params->data;
		delete params;
	}
}

static void DInput5Relay2Napi_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	DInput5Relay2Napi* module = (DInput5Relay2Napi*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	// Call to the client node javascript function must be by means of a NapiThreadSafe BlockingCall
	module->callback.BlockingCall(params, DInput5Relay2Napi_mainthread_callback);
}

Napi::Object DInput5Relay2Napi::init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "DInput5Relay2Napi",
									  {StaticMethod("checkModulePresent", &DInput5Relay2Napi::checkModulePresent),
									   InstanceMethod("configPulseWidth", &DInput5Relay2Napi::configPulseWidth),
									   InstanceMethod("activate", &DInput5Relay2Napi::activate),
									   InstanceMethod("deactivate", &DInput5Relay2Napi::deactivate),
									   InstanceMethod("activateAll", &DInput5Relay2Napi::activateAll),
									   InstanceMethod("deactivateAll", &DInput5Relay2Napi::deactivateAll),
									   InstanceMethod("setStatus", &DInput5Relay2Napi::setStatus),

									   InstanceMethod("getRelayStatus", &DInput5Relay2Napi::getRelayStatus),
									   InstanceMethod("getAllRelayStatus", &DInput5Relay2Napi::getAllRelayStatus),

									   InstanceMethod("getInputStatus", &DInput5Relay2Napi::getInputStatus),
									   InstanceMethod("getAllInputStatus", &DInput5Relay2Napi::getAllInputStatus),
									   InstanceMethod("setPulseFilterTime", &DInput5Relay2Napi::setPulseFilterTime),
									   InstanceMethod("getPulseCount", &DInput5Relay2Napi::getPulseCount),
									   InstanceMethod("getAllPulseCount", &DInput5Relay2Napi::getAllPulseCount),
									   InstanceMethod("resetPulseCount", &DInput5Relay2Napi::resetPulseCount),
									   InstanceMethod("resetAllPulseCount", &DInput5Relay2Napi::resetAllPulseCount),
									   InstanceMethod("switchToMode", &DInput5Relay2Napi::switchToMode),
									   InstanceMethod("config_event_at_time_interval", &DInput5Relay2Napi::config_event_at_time_interval),
									   InstanceMethod("configEventOnNewData", &DInput5Relay2Napi::configEventOnNewData),
									   InstanceMethod("configEventOnValueChange", &DInput5Relay2Napi::configEventOnValueChange),
									   InstanceMethod("resetEventConfig", &DInput5Relay2Napi::resetEventConfig),
									   InstanceMethod("getFunctionsOffsetId", &DInput5Relay2Napi::getFunctionsOffsetId),
									   InstanceMethod("getInputsOffsetId", &DInput5Relay2Napi::getInputsOffsetId),
									   StaticValue("ALL_INPUT", Napi::Number::New(env, DInput5Relay2Module::DI_ALL_INPUT)),
									   StaticValue("MODE_PULSE_COUNTER", Napi::Number::New(env, DInput5Relay2Module::MODE_PULSE_COUNTER)),
									   StaticValue("MODE_WIDTH_COUNTER", Napi::Number::New(env, DInput5Relay2Module::MODE_WIDTH_COUNTER))});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

DInput5Relay2Napi::DInput5Relay2Napi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<DInput5Relay2Napi>(info) {
	Napi::Env env = info.Env();
	DInput5Relay2ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "DInput5Relay2Napi_callback", 0, 1);
		callback_func = DInput5Relay2Napi_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new DInput5Relay2Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 5DI+2PR module").ThrowAsJavaScriptException();
		return;
	}
}

DInput5Relay2Napi::~DInput5Relay2Napi() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value DInput5Relay2Napi::checkModulePresent(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeDI5PR2, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void DInput5Relay2Napi::configPulseWidth(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes exactly two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return;
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 2) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return;
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "width_ms parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number width_ms = info[1].As<Napi::Number>();

	if (this->module->configPulseWidth(relay_mask, width_ms.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting pulse width").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput5Relay2Napi::activate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput5Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->activate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_RELAYS);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_RELAYS; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput5Relay2Napi::deactivate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != DInput5Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->deactivate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_RELAYS);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_RELAYS; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput5Relay2Napi::setStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;
	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "status_array parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();
	if (relay_mask_array.Length() != DInput5Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "status_array parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_RELAYS);

	uint8_t activate_mask = 0;
	uint8_t deactivate_mask = 0;
	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (!v.IsNull()) {
			if (v.ToBoolean().Value()) {
				activate_mask |= 1 << i;
			} else {
				deactivate_mask |= 1 << i;
			}
		}
	}

	if (this->module->activate(activate_mask) == 0) {
		if (this->module->deactivate(deactivate_mask) == 0) {
			HAL::sleepMs(20);
			if (this->module->getAllRelayStatus(&status) == 0) {
				for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_RELAYS; i++) {
					status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
				}
				return status_array;
			}
		}
	}

	Napi::TypeError::New(env, "Error setting relay status").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput5Relay2Napi::activateAll(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->activateAll() != 0) {
		Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_RELAYS);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_RELAYS; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput5Relay2Napi::deactivateAll(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->deactivateAll() != 0) {
		Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_RELAYS);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_RELAYS; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value DInput5Relay2Napi::getRelayStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 2) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	uint8_t status = 0;

	if (this->module->getRelayStatus(relay_mask, &status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env);

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

Napi::Value DInput5Relay2Napi::getAllRelayStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->getAllRelayStatus(&status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 2);

	for (uint32_t i = 0; i < 2; i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

Napi::Value DInput5Relay2Napi::getInputStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	uint8_t status;

	if (this->module->getStatus(input_number.Uint32Value(), &status) != 0) {
		Napi::TypeError::New(env, "Error getting digital input status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, status != 0);
}

Napi::Value DInput5Relay2Napi::getAllInputStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t status[DInput5Relay2Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllStatus(status) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_DI_INPUTS; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] != 0);
	}

	return status_array;
}

void DInput5Relay2Napi::setPulseFilterTime(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (info.Length() < 2 && !info[1].IsNumber()) {
		Napi::TypeError::New(env, "ms_time parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_time = info[1].As<Napi::Number>();

	if (this->module->setPulseFilterTime(input_number.Uint32Value(), ms_time.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting digital input pulse filter time").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput5Relay2Napi::getPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t count;

	if (this->module->getPulseCount(input_number.Uint32Value(), &count) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, count);
}

Napi::Value DInput5Relay2Napi::getAllPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t counts[DInput5Relay2Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllPulseCount(counts) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array counts_array = Napi::Array::New(env, DInput5Relay2Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput5Relay2Module::NUMBER_OF_DI_INPUTS; i++) {
		counts_array[i] = Napi::Number::New(env, counts[i]);
	}

	return counts_array;
}

void DInput5Relay2Napi::resetPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (this->module->resetPulseCount(input_number.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::resetAllPulseCount(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->resetAllPulseCount() != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::switchToMode(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes exactly two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "mode parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number mode = info[1].As<Napi::Number>();

	if (this->module->switchToMode(input_number.Uint32Value(), mode.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error switching mode").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::config_event_at_time_interval(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_out = info[0].As<Napi::Number>();
	if (this->module->configEventAtTimeInterval(time_out.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::configEventOnNewData(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->configEventOnNewData() != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::configEventOnValueChange(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one parameter").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput5Relay2Module::DI_ALL_INPUT);
	}

	if (this->module->configEventOnValueChange(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void DInput5Relay2Napi::resetEventConfig(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	Napi::Boolean update_module;

	if (info.Length() > 0) {
		update_module = info[0].As<Napi::Boolean>();
	} else {
		update_module = Napi::Boolean::New(env, true);
	}

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "Error resetting event config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value DInput5Relay2Napi::getInputsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getInputsOffsetId();
	return Napi::Number::New(env, offset_id);
}

Napi::Value DInput5Relay2Napi::getFunctionsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getFunctionsOffsetId();
	return Napi::Number::New(env, offset_id);
}