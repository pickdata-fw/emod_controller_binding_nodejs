#ifndef AINPUT12_MODULE_H
#define AINPUT12_MODULE_H

#include <napi.h>

#include "AInput12Module.hpp"

class AInput12Napi : public Napi::ObjectWrap<AInput12Napi> {
public:
	Napi::ThreadSafeFunction callback;

	static Napi::Object Init(Napi::Env env);
	AInput12Napi(const Napi::CallbackInfo& info);
	~AInput12Napi();

private:
	static Napi::Value check_module_present(const Napi::CallbackInfo& info);

	// AI methods
	void config_sample_rate(const Napi::CallbackInfo& info);
	void config_input(const Napi::CallbackInfo& info);
	Napi::Value get_input_config(const Napi::CallbackInfo& info);
	Napi::Value get_analog_input(const Napi::CallbackInfo& info);
	Napi::Value get_all_analog_input(const Napi::CallbackInfo& info);

	// Callback methods
	void config_event_at_time_interval(const Napi::CallbackInfo& info);
	void config_event_on_value_change(const Napi::CallbackInfo& info);
	void config_event_within_range(const Napi::CallbackInfo& info);
	void config_event_out_of_range(const Napi::CallbackInfo& info);
	void reset_event_config(const Napi::CallbackInfo& info);
	Napi::Value voltage_to_samples(const Napi::CallbackInfo& info);
	Napi::Value current_to_samples(const Napi::CallbackInfo& info);
	Napi::Value samples_to_voltage(const Napi::CallbackInfo& info);
	Napi::Value samples_to_current(const Napi::CallbackInfo& info);
	Napi::Value getInputsOffsetId(const Napi::CallbackInfo& info);
	Napi::Value getNumAnalogInputs(const Napi::CallbackInfo& info);

	AInput12Module* module;
	bool polling_mode;
	static Napi::FunctionReference constructor;
};

#endif