#ifndef _PowerStatusNapi_hpp_
#define _PowerStatusNapi_hpp_

#include <napi.h>

#include "PowerStatus.hpp"

class PowerStatusNapi : public Napi::ObjectWrap<PowerStatusNapi> {
	PowerStatus* pwr;
	static Napi::FunctionReference constructor;

public:
	bool external_power;

	Napi::ThreadSafeFunction statusChangeCallback;
	Napi::Value statusChangeCtx;

	Napi::ThreadSafeFunction failCallback;
	Napi::Value failCtx;

protected:
	static Napi::Value checkModulePresent(const Napi::CallbackInfo& info);
	void setStatusChangeCallback(const Napi::CallbackInfo& info);
	void setFailCallback(const Napi::CallbackInfo& info);

public:
	static Napi::Object Init(Napi::Env env);
	PowerStatusNapi(const Napi::CallbackInfo& info);
	~PowerStatusNapi();
};

#endif