#include "relay_module.hpp"

#include "Controller.hpp"
#include "HAL.hpp"
Napi::FunctionReference relay_module::constructor;

Napi::Function relay_module::Init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "relay_module",
									  {
										  StaticMethod("check_module_present", &relay_module::check_module_present),
										  InstanceMethod("config_pulse_width", &relay_module::config_pulse_width),
										  InstanceMethod("activate", &relay_module::activate),
										  InstanceMethod("deactivate", &relay_module::deactivate),
										  InstanceMethod("activate_all", &relay_module::activate_all),
										  InstanceMethod("deactivate_all", &relay_module::deactivate_all),
										  InstanceMethod("setStatus", &relay_module::setStatus),
										  InstanceMethod("get_relay_status", &relay_module::get_relay_status),
										  InstanceMethod("get_all_relay_status", &relay_module::get_all_relay_status),
									  });

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

relay_module::relay_module(const Napi::CallbackInfo& info) : Napi::ObjectWrap<relay_module>(info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new Relay8Module();
	if (this->module->init(variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 8SR module").ThrowAsJavaScriptException();
		return;
	}
}

relay_module::~relay_module() { delete this->module; }

Napi::Value relay_module::check_module_present(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeR8, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void relay_module::config_pulse_width(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes exactly two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return;
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 8) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 8 elements").ThrowAsJavaScriptException();
		return;
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "width_ms parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number width_ms = info[1].As<Napi::Number>();

	if (this->module->configPulseWidth(relay_mask, width_ms.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting pulse width").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value relay_module::activate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 8) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 8 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->activate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, 8);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < 8; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value relay_module::deactivate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 8) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 8 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->deactivate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, 8);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < 8; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value relay_module::setStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;
	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "status_array parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();
	if (relay_mask_array.Length() != 8) {
		Napi::TypeError::New(env, "status_array parameter must be an array of 8 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	Napi::Array status_array = Napi::Array::New(env, 8);

	uint8_t activate_mask = 0;
	uint8_t deactivate_mask = 0;
	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (!v.IsNull()) {
			if (v.ToBoolean().Value()) {
				activate_mask |= 1 << i;
			} else {
				deactivate_mask |= 1 << i;
			}
		}
	}

	if (this->module->activate(activate_mask) == 0) {
		if (this->module->deactivate(deactivate_mask) == 0) {
			HAL::sleepMs(20);
			if (this->module->getAllRelayStatus(&status) == 0) {
				for (uint32_t i = 0; i < 8; i++) {
					status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
				}
				return status_array;
			}
		}
	}

	Napi::TypeError::New(env, "Error setting relay status").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value relay_module::activate_all(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->activateAll() != 0) {
		Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 8);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < 8; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value relay_module::deactivate_all(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->deactivateAll() != 0) {
		Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 8);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < 8; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value relay_module::get_relay_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 8) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 8 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	uint8_t status = 0;

	if (this->module->getRelayStatus(relay_mask, &status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env);

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

Napi::Value relay_module::get_all_relay_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->getAllRelayStatus(&status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 8);

	for (uint32_t i = 0; i < 8; i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}
