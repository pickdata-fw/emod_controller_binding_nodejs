#ifndef _EnergyMeter3Napi_hpp_
#define _EnergyMeter3Napi_hpp_

#include <napi.h>

#include <string>

#include "CommonNapi.hpp"
#include "EnergyMeter3Module.hpp"

class EnergyMeter3Napi : public Napi::ObjectWrap<EnergyMeter3Napi>, public CommonNapi {
public:
	Napi::ThreadSafeFunction callback;

	ModuleType em3_type_;
	static Napi::Object initWrapper(Napi::Env env);
	EnergyMeter3Napi(const Napi::CallbackInfo& info);
	~EnergyMeter3Napi();

protected:
	EnergyMeter3Module* module_;
	bool polling_mode_;
	static Napi::Value checkModulePresent(const Napi::CallbackInfo& info);

	// Napi::Value initModule(const Napi::CallbackInfo& info);
	Napi::Value configModule(const Napi::CallbackInfo& info);

	Napi::Value resetAllEnergyMeters(const Napi::CallbackInfo& info);

	Napi::Value getCurrentFullScale(const Napi::CallbackInfo& info);
	Napi::Value getVoltageFullScale(const Napi::CallbackInfo& info);
	Napi::Value getCurrentDirection(const Napi::CallbackInfo& info);

	Napi::Value getWorkMode(const Napi::CallbackInfo& info);
	Napi::Value getMeteringStandard(const Napi::CallbackInfo& info);

	Napi::Value getPowerParameters(const Napi::CallbackInfo& info);
	Napi::Value getEnergyParameters(const Napi::CallbackInfo& info);
	Napi::Value getCombinedPowerParameters(const Napi::CallbackInfo& info);
	Napi::Value getCombinedEnergyParameters(const Napi::CallbackInfo& info);

	// DEPRECATED, to be deleted --------------------------------------------------
	Napi::Value getAllParameters(const Napi::CallbackInfo& info);
	Napi::Value getPowerParametersOld(const Napi::CallbackInfo& info, int phase);
	Napi::Value getEnergyParametersOld(const Napi::CallbackInfo& info, int phase);
	Napi::Value getPowerParameters_L1(const Napi::CallbackInfo& info);
	Napi::Value getPowerParameters_L2(const Napi::CallbackInfo& info);
	Napi::Value getPowerParameters_L3(const Napi::CallbackInfo& info);
	Napi::Value getPowerParameters_L123(const Napi::CallbackInfo& info);
	Napi::Value getEnergyParameters_L1(const Napi::CallbackInfo& info);
	Napi::Value getEnergyParameters_L2(const Napi::CallbackInfo& info);
	Napi::Value getEnergyParameters_L3(const Napi::CallbackInfo& info);
	Napi::Value getEnergyParameters_L123(const Napi::CallbackInfo& info);
	//-----------------------------------------------------------------------------

	Napi::Value configEventAtTimeInterval(const Napi::CallbackInfo& info);
	Napi::Value resetEventConfig(const Napi::CallbackInfo& info);
};

#endif