#include "PowerStatusNapi.hpp"

#include <cstring>

#include "Controller.hpp"
#include "HAL.hpp"

Napi::FunctionReference PowerStatusNapi::constructor;

static void PowerStatusNapi_mainthread_statusChangeCallback(Napi::Env env, Napi::Function jsCallback, PowerStatusNapi* pwr) {
	Napi::Boolean external_power = Napi::Boolean::New(env, pwr->external_power);

	jsCallback.Call({external_power, pwr->statusChangeCtx});
}

static void PowerStatusNapi_statusChangeCallback(bool external_power, void* ctx) {
	PowerStatusNapi* pwr = (PowerStatusNapi*)ctx;

	pwr->external_power = external_power;
	pwr->statusChangeCallback.BlockingCall(pwr, PowerStatusNapi_mainthread_statusChangeCallback);
}

static void PowerStatusNapi_mainthread_failCallback(Napi::Env env, Napi::Function jsCallback, PowerStatusNapi* pwr) { jsCallback.Call({pwr->failCtx}); }

static void PowerStatusNapi_failCallback(void* ctx) {
	PowerStatusNapi* pwr = (PowerStatusNapi*)ctx;

	pwr->statusChangeCallback.BlockingCall(pwr, PowerStatusNapi_mainthread_failCallback);
}

Napi::Object PowerStatusNapi::Init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "PowerStatusNapi",
									  {StaticMethod("checkModulePresent", &PowerStatusNapi::checkModulePresent),
									   InstanceMethod("setStatusChangeCallback", &PowerStatusNapi::setStatusChangeCallback),
									   InstanceMethod("setFailCallback", &PowerStatusNapi::setFailCallback)});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

PowerStatusNapi::PowerStatusNapi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<PowerStatusNapi>(info) {
	Napi::Env env = info.Env();

	this->statusChangeCtx = env.Null();
	this->failCtx = env.Null();
	this->pwr = &HAL::getPowerStatus();
}

PowerStatusNapi::~PowerStatusNapi() {}

Napi::Value PowerStatusNapi::checkModulePresent(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeSupercap, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void PowerStatusNapi::setStatusChangeCallback(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsFunction()) {
		Napi::TypeError::New(env, "First parameter must be a function").ThrowAsJavaScriptException();
		return;
	}

	this->statusChangeCallback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "PowerStatusNapi_statusChangeCallback", 0, 1);

	if (info.Length() > 1) {
		this->statusChangeCtx = info[1];
	} else {
		this->statusChangeCtx = env.Null();
	}

	if (this->pwr->setStatusChangeCallback(PowerStatusNapi_statusChangeCallback, this) != 0) {
		Napi::TypeError::New(env, "Error setting status change callback").ThrowAsJavaScriptException();
		return;
	}
}

void PowerStatusNapi::setFailCallback(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsFunction()) {
		Napi::TypeError::New(env, "First parameter must be a function").ThrowAsJavaScriptException();
		return;
	}

	this->failCallback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "PowerStatusNapi_failCallback", 0, 1);

	if (info.Length() > 1) {
		this->failCtx = info[1];
	} else {
		this->failCtx = env.Null();
	}

	if (this->pwr->setFailCallback(PowerStatusNapi_failCallback, this) != 0) {
		Napi::TypeError::New(env, "Error setting failz callback").ThrowAsJavaScriptException();
		return;
	}
}
