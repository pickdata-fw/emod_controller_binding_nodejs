#include "analog_inputs_relay_module.hpp"

#include <cstring>

#include "Controller.hpp"

Napi::FunctionReference analog_inputs_relay_module::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void analog_inputs_relay_module_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void analog_inputs_relay_module_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	analog_inputs_relay_module* module = (analog_inputs_relay_module*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	module->callback.BlockingCall(params, analog_inputs_relay_module_mainthread_callback);
}

Napi::Object analog_inputs_relay_module::Init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "analog_inputs_relay_module",
									  {StaticMethod("check_module_present", &analog_inputs_relay_module::check_module_present),
									   InstanceMethod("config_sample_rate", &analog_inputs_relay_module::config_sample_rate),
									   InstanceMethod("config_input", &analog_inputs_relay_module::config_input),
									   InstanceMethod("get_input_config", &analog_inputs_relay_module::get_input_config),
									   InstanceMethod("get_analog_input", &analog_inputs_relay_module::get_analog_input),
									   InstanceMethod("get_all_analog_input", &analog_inputs_relay_module::get_all_analog_input),
									   InstanceMethod("config_pulse_width", &analog_inputs_relay_module::config_pulse_width),
									   InstanceMethod("activate", &analog_inputs_relay_module::activate),
									   InstanceMethod("deactivate", &analog_inputs_relay_module::deactivate),
									   InstanceMethod("activate_all", &analog_inputs_relay_module::activate_all),
									   InstanceMethod("deactivate_all", &analog_inputs_relay_module::deactivate_all),
									   InstanceMethod("setStatus", &analog_inputs_relay_module::setStatus),

									   InstanceMethod("get_relay_status", &analog_inputs_relay_module::get_relay_status),
									   InstanceMethod("get_all_relay_status", &analog_inputs_relay_module::get_all_relay_status),
									   InstanceMethod("config_event_at_time_interval", &analog_inputs_relay_module::config_event_at_time_interval),
									   InstanceMethod("config_event_on_value_change", &analog_inputs_relay_module::config_event_on_value_change),
									   InstanceMethod("config_event_within_range", &analog_inputs_relay_module::config_event_within_range),
									   InstanceMethod("config_event_out_of_range", &analog_inputs_relay_module::config_event_out_of_range),
									   InstanceMethod("reset_event_config", &analog_inputs_relay_module::reset_event_config),
									   InstanceMethod("voltage_to_samples", &analog_inputs_relay_module::voltage_to_samples),
									   InstanceMethod("current_to_samples", &analog_inputs_relay_module::current_to_samples),
									   InstanceMethod("samples_to_voltage", &analog_inputs_relay_module::samples_to_voltage),
									   InstanceMethod("samples_to_current", &analog_inputs_relay_module::samples_to_current),
									   InstanceMethod("getInputsOffsetId", &analog_inputs_relay_module::getInputsOffsetId),
									   InstanceMethod("getNumAnalogInputs", &analog_inputs_relay_module::getNumAnalogInputs),
									   StaticValue("input01", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT01)),
									   StaticValue("input02", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT02)),
									   StaticValue("input03", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT03)),
									   StaticValue("input04", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT04)),
									   StaticValue("input05", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT05)),
									   StaticValue("input06", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT06)),
									   StaticValue("input07", Napi::Number::New(env, AInput7Relay2Module::AI_INPUT07)),
									   StaticValue("all_input", Napi::Number::New(env, AInput7Relay2Module::AI_ALL_INPUT)),
									   StaticValue("max_inputs", Napi::Number::New(env, AInput7Relay2Module::NUMBER_OF_AI_INPUTS))});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

analog_inputs_relay_module::analog_inputs_relay_module(const Napi::CallbackInfo& info) : Napi::ObjectWrap<analog_inputs_relay_module>(info) {
	Napi::Env env = info.Env();
	AInput7Relay2ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "analog_inputs_relay_module_callback", 0, 1);
		callback_func = analog_inputs_relay_module_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new AInput7Relay2Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 7AI+2PR module").ThrowAsJavaScriptException();
		return;
	}
}

analog_inputs_relay_module::~analog_inputs_relay_module() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value analog_inputs_relay_module::check_module_present(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeAI7PR2, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void analog_inputs_relay_module::config_sample_rate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "ms_period parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_period = info[0].As<Napi::Number>();

	if (this->module->configSampleRate(ms_period.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting analog inputs sample rate").ThrowAsJavaScriptException();
		return;
	}
}

void analog_inputs_relay_module::config_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "voltage_input_mask parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number voltage_input_mask = info[0].As<Napi::Number>();

	if (this->module->configInput(voltage_input_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting analog input config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value analog_inputs_relay_module::get_input_config(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint16_t config;

	if (this->module->getInputConfig(&config) != 0) {
		Napi::TypeError::New(env, "Error getting analog input config").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, config);
}

Napi::Value analog_inputs_relay_module::get_analog_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	float data;

	if (this->module->getAnalogInput(input_number.Uint32Value(), &data) != 0) {
		Napi::TypeError::New(env, "Error getting analog input value").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, data);
}

Napi::Value analog_inputs_relay_module::get_all_analog_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	float data[AInput7Relay2Module::NUMBER_OF_AI_INPUTS];

	if (this->module->getAllAnalogInput(data) != 0) {
		Napi::TypeError::New(env, "Error getting analog inputs value").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array data_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_AI_INPUTS);

	for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_AI_INPUTS; i++) {
		data_array[i] = Napi::Number::New(env, data[i]);
	}

	return data_array;
}

void analog_inputs_relay_module::config_pulse_width(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes exactly two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return;
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 2) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return;
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "width_ms parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number width_ms = info[1].As<Napi::Number>();

	if (this->module->configPulseWidth(relay_mask, width_ms.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting pulse width").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value analog_inputs_relay_module::activate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != AInput7Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->activate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_RELAYS);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_RELAYS; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value analog_inputs_relay_module::deactivate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != AInput7Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	if (this->module->deactivate(relay_mask) == 0) {
		HAL::sleepMs(20);
		uint8_t status = 0;
		Napi::Array status_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_RELAYS);
		if (this->module->getAllRelayStatus(&status) == 0) {
			for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_RELAYS; i++) {
				status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
			}
			return status_array;
		}
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value analog_inputs_relay_module::setStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;
	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "status_array parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();
	if (relay_mask_array.Length() != AInput7Relay2Module::NUMBER_OF_RELAYS) {
		Napi::TypeError::New(env, "status_array parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	Napi::Array status_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_RELAYS);

	uint8_t activate_mask = 0;
	uint8_t deactivate_mask = 0;
	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (!v.IsNull()) {
			if (v.ToBoolean().Value()) {
				activate_mask |= 1 << i;
			} else {
				deactivate_mask |= 1 << i;
			}
		}
	}

	if (this->module->activate(activate_mask) == 0) {
		if (this->module->deactivate(deactivate_mask) == 0) {
			HAL::sleepMs(20);
			if (this->module->getAllRelayStatus(&status) == 0) {
				for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_RELAYS; i++) {
					status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
				}
				return status_array;
			}
		}
	}

	Napi::TypeError::New(env, "Error setting relay status").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value analog_inputs_relay_module::activate_all(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->activateAll() != 0) {
		Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_RELAYS);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_RELAYS; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error activating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value analog_inputs_relay_module::deactivate_all(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->deactivateAll() != 0) {
		Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, AInput7Relay2Module::NUMBER_OF_RELAYS);
	HAL::sleepMs(20);
	if (this->module->getAllRelayStatus(&status) == 0) {
		for (uint32_t i = 0; i < AInput7Relay2Module::NUMBER_OF_RELAYS; i++) {
			status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
		}
		return status_array;
	}

	Napi::TypeError::New(env, "Error deactivating relays").ThrowAsJavaScriptException();
	return env.Undefined();
}

Napi::Value analog_inputs_relay_module::get_relay_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsArray()) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array relay_mask_array = info[0].As<Napi::Array>();

	if (relay_mask_array.Length() != 2) {
		Napi::TypeError::New(env, "relay_mask parameter must be an array of 2 elements").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	uint8_t relay_mask = 0;

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		Napi::Value v = relay_mask_array[i];
		if (v.ToBoolean().Value()) {
			relay_mask |= 1 << i;
		}
	}

	uint8_t status = 0;

	if (this->module->getRelayStatus(relay_mask, &status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 2);

	for (uint32_t i = 0; i < relay_mask_array.Length(); i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

Napi::Value analog_inputs_relay_module::get_all_relay_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t status = 0;

	if (this->module->getAllRelayStatus(&status) != 0) {
		Napi::TypeError::New(env, "Error getting relay status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, 2);

	for (uint32_t i = 0; i < 2; i++) {
		status_array[i] = Napi::Boolean::New(env, ((status & (1 << i)) != 0));
	}

	return status_array;
}

void analog_inputs_relay_module::config_event_at_time_interval(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_out = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput7Relay2Module::AI_ALL_INPUT);
	}

	if (this->module->configEventAtTimeInterval(time_out.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void analog_inputs_relay_module::config_event_on_value_change(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "threshold parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput7Relay2Module::AI_ALL_INPUT);
	}

	if (this->module->configEventOnValueChange(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void analog_inputs_relay_module::config_event_within_range(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput7Relay2Module::AI_ALL_INPUT);
	}

	if (this->module->configEventWithinRange(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void analog_inputs_relay_module::config_event_out_of_range(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput7Relay2Module::AI_ALL_INPUT);
	}

	if (this->module->configEventOutOfRange(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void analog_inputs_relay_module::reset_event_config(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	Napi::Boolean update_module;

	if (info.Length() > 0) {
		update_module = info[0].As<Napi::Boolean>();
	} else {
		update_module = Napi::Boolean::New(env, true);
	}

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "Error resetting event config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value analog_inputs_relay_module::voltage_to_samples(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number voltage = info[0].As<Napi::Number>();

	uint16_t samples = module->voltageToSamples(voltage.FloatValue());

	return Napi::Number::New(env, samples);
}

Napi::Value analog_inputs_relay_module::current_to_samples(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number current = info[0].As<Napi::Number>();

	uint16_t samples = module->currentToSamples(current.FloatValue());

	return Napi::Number::New(env, samples);
}

Napi::Value analog_inputs_relay_module::samples_to_voltage(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number samples = info[0].As<Napi::Number>();

	float voltage = module->samplesToVoltage(samples.Uint32Value());

	return Napi::Number::New(env, voltage);
}

Napi::Value analog_inputs_relay_module::samples_to_current(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number samples = info[0].As<Napi::Number>();

	float current = module->samplesToCurrent(samples.Uint32Value());

	return Napi::Number::New(env, current);
}

Napi::Value analog_inputs_relay_module::getInputsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getInputsOffsetId();
	return Napi::Number::New(env, offset_id);
}

Napi::Value analog_inputs_relay_module::getNumAnalogInputs(const Napi::CallbackInfo& info) {
	uint8_t num_inputs;
	Napi::Env env = info.Env();

	num_inputs = module->getNumAnalogInputs();
	return Napi::Number::New(env, num_inputs);
}