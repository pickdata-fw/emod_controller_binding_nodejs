#ifndef DINPUT_MODULE_H
#define DINPUT_MODULE_H

#include <napi.h>

#include "DInput10Module.hpp"

class digital_inputs_module : public Napi::ObjectWrap<digital_inputs_module> {
public:
	Napi::ThreadSafeFunction callback;

	static Napi::Object Init(Napi::Env env);
	digital_inputs_module(const Napi::CallbackInfo& info);
	~digital_inputs_module();

private:
	static Napi::Value check_module_present(const Napi::CallbackInfo& info);

	Napi::Value get_status(const Napi::CallbackInfo& info);
	Napi::Value get_all_status(const Napi::CallbackInfo& info);
	void set_pulse_filter_time(const Napi::CallbackInfo& info);
	Napi::Value get_pulse_filter_time(const Napi::CallbackInfo& info);
	Napi::Value get_all_pulse_filter_time(const Napi::CallbackInfo& info);
	Napi::Value get_pulse_count(const Napi::CallbackInfo& info);
	Napi::Value get_all_pulse_count(const Napi::CallbackInfo& info);
	void reset_pulse_count(const Napi::CallbackInfo& info);
	void reset_all_pulse_count(const Napi::CallbackInfo& info);
	Napi::Value get_pulse_width(const Napi::CallbackInfo& info);
	Napi::Value get_all_pulse_width(const Napi::CallbackInfo& info);

	void switch_to_mode(const Napi::CallbackInfo& info);
	void config_event_at_time_interval(const Napi::CallbackInfo& info);
	void config_event_on_new_data(const Napi::CallbackInfo& info);
	void config_event_on_value_change(const Napi::CallbackInfo& info);
	void reset_event_config(const Napi::CallbackInfo& info);
	Napi::Value getInputsOffsetId(const Napi::CallbackInfo& info);
	Napi::Value getFunctionsOffsetId(const Napi::CallbackInfo& info);

	DInput10Module* module;
	bool polling_mode;
	static Napi::FunctionReference constructor;
};

#endif