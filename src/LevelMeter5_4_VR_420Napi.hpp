#ifndef _LevelMeter5_4_VR_420Napi_hpp_
#define _LevelMeter5_4_VR_420Napi_hpp_

#include <napi.h>

#include "LevelMeter5_4_VR_420Module.hpp"

class LevelMeter5_4_VR_420Napi : public Napi::ObjectWrap<LevelMeter5_4_VR_420Napi> {
public:
	Napi::ThreadSafeFunction callback;

	static Napi::Object Init(Napi::Env env);
	LevelMeter5_4_VR_420Napi(const Napi::CallbackInfo& info);
	~LevelMeter5_4_VR_420Napi();

private:
	LevelMeter5_4_VR_420Module* module;
	bool polling_mode;
	static Napi::FunctionReference constructor;

	static Napi::Value checkModulePresent(const Napi::CallbackInfo& info);

	void configResistiveSensorPolarity(const Napi::CallbackInfo& info);
	void configAllResistiveSensorPolarity(const Napi::CallbackInfo& info);

	void configCurrentSensorPolarity(const Napi::CallbackInfo& info);
	void configAllCurrentSensorPolarity(const Napi::CallbackInfo& info);

	Napi::Value getResistiveSensorPolarity(const Napi::CallbackInfo& info);
	Napi::Value getAllResistiveSensorPolarity(const Napi::CallbackInfo& info);

	Napi::Value getCurrentSensorPolarity(const Napi::CallbackInfo& info);
	Napi::Value getAllCurrentSensorPolarity(const Napi::CallbackInfo& info);

	void setResistiveSensorThreshold(const Napi::CallbackInfo& info);
	void setAllResistiveSensorThreshold(const Napi::CallbackInfo& info);

	void setResistiveSensorSensitivity(const Napi::CallbackInfo& info);
	void setAllResistiveSensorSensitivity(const Napi::CallbackInfo& info);

	void setCurrentSensorType(const Napi::CallbackInfo& info);
	void setAllCurrentSensorType(const Napi::CallbackInfo& info);

	Napi::Value getResistiveLevelStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllResistiveLevelStatus(const Napi::CallbackInfo& info);

	Napi::Value getCurrentLevelStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllCurrentLevelStatus(const Napi::CallbackInfo& info);

	Napi::Value getPotentiometerInput(const Napi::CallbackInfo& info);

	Napi::Value get420Input(const Napi::CallbackInfo& info);

	void configEventOnNewData(const Napi::CallbackInfo& info);
	void configEventAtTimeIntervalLevel(const Napi::CallbackInfo& info);

	void configEventWithinRangePotentiometer(const Napi::CallbackInfo& info);
	void configEventOutOfRangePotentiometer(const Napi::CallbackInfo& info);
	void configEventOnValueChangePotentiometer(const Napi::CallbackInfo& info);
	void configEventAtTimeIntervalPotentiometer(const Napi::CallbackInfo& info);

	void configEventWithinRange420(const Napi::CallbackInfo& info);
	void configEventOutOfRange420(const Napi::CallbackInfo& info);
	void configEventOnValueChange420(const Napi::CallbackInfo& info);
	void configEventAtTimeInterval420(const Napi::CallbackInfo& info);

	void resetEventConfig(const Napi::CallbackInfo& info);
	Napi::Value getFunctionsOffsetId(const Napi::CallbackInfo& info);
};

#endif