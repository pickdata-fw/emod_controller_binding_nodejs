#ifndef _CommonNapi_hpp_
#define _CommonNapi_hpp_

#include <string>

#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "Module.hpp"
#include "napi.h"

class CommonNapi {
protected:
	bool javaScriptExceptionsActive;
	bool moduleInitialized;

	Napi::Value throwJavaScriptException(const Napi::Env& env, const char* process_prefix, const char* process, ModuleType m, EmodRet errCode);

	EmodRet getFloatFromKey(const Napi::Object& objectJSON, const char* key, float& float_value);
	EmodRet getUIntFromKey(const Napi::Object& objectJSON, const char* key, uint32_t& uint32_value);
	EmodRet getUIntArrayFromKey(const Napi::Object& objectJSON, const char* key, std::vector<uint32_t>& values);
	EmodRet getFloatElement(const Napi::Object& array_object, uint32_t i, float& float_value);
	EmodRet getBoolElement(const Napi::Object& array_object, uint32_t i, bool& bool_value);
	EmodRet getFloatElementFromKey(const Napi::Object& objectJSON, const char* key, int i, float& float_value);
	EmodRet getBoolElementFromKey(const Napi::Object& objectJSON, const char* key, int i, bool& bool_value);
	EmodRet getArrayObjectFromKey(const Napi::Object& objectJSON, const char* key, Napi::Object& object_out);
	EmodRet getParametersObject(const Napi::CallbackInfo& info, const int arg_num, Napi::Object& object_parameter);
	EmodRet getParametersNumber(const Napi::CallbackInfo& info, const int arg_num, Napi::Value& number_parameter);
	EmodRet getParameters(const Napi::CallbackInfo& info, const int arg_num, Napi::Value& parameter);

	std::string FloatToString(float float_value, int prec);

public:
	CommonNapi();
};

#endif