#include "EnergyMeter3Napi.hpp"

#include <vector>

#include "Controller.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "EnergyMeterParameters.hpp"

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void threadSafeModuleCallbackWrapper(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	// TODO: convert output to string, similar to getPowerParameters and other Napi getters in this class.
	PhasePowerParameters power;
	CombinedPowerParameters power123;
	EnergyParameters energy;

	Napi::Object callback_parameters = Napi::Object::New(env);

	switch (params->idFunction) {
		case 1:
		case 2:
		case 3:
			power = *(PhasePowerParameters*)params->data;

			callback_parameters.Set(std::string("voltage"), power.voltage);
			callback_parameters.Set(std::string("current"), power.current);
			callback_parameters.Set(std::string("frequency"), power.frequency);
			callback_parameters.Set(std::string("cosine"), power.cosine);
			callback_parameters.Set(std::string("angle"), power.angle);
			callback_parameters.Set(std::string("active_power"), power.active_power);
			callback_parameters.Set(std::string("aparent_power"), power.aparent_power);
			callback_parameters.Set(std::string("reactive_power"), power.reactive_power);
			callback_parameters.Set(std::string("exported_active_power"), power.exported_active_power);
			callback_parameters.Set(std::string("exported_aparent_power"), power.exported_aparent_power);
			callback_parameters.Set(std::string("exported_inductive_power"), power.exported_inductive_power);
			callback_parameters.Set(std::string("exported_capacitive_power"), power.exported_capacitive_power);
			callback_parameters.Set(std::string("imported_active_power"), power.imported_active_power);
			callback_parameters.Set(std::string("imported_aparent_power"), power.imported_aparent_power);
			callback_parameters.Set(std::string("imported_inductive_power"), power.imported_inductive_power);
			callback_parameters.Set(std::string("imported_capacitive_power"), power.imported_capacitive_power);
			callback_parameters.Set(std::string("maximeter"), power.maximeter);
			break;
		case 4:
			power123 = *(CombinedPowerParameters*)params->data;

			callback_parameters.Set(std::string("neutral_current"), power123.neutral_current);
			callback_parameters.Set(std::string("power_factor"), power123.power_factor);
			callback_parameters.Set(std::string("neutral_angle"), power123.neutral_angle);
			callback_parameters.Set(std::string("phase_rotation"), power123.phase_rotation);
			callback_parameters.Set(std::string("active_power"), power123.active_power);
			callback_parameters.Set(std::string("aparent_power"), power123.aparent_power);
			callback_parameters.Set(std::string("reactive_power"), power123.reactive_power);
			callback_parameters.Set(std::string("exported_active_power"), power123.exported_active_power);
			callback_parameters.Set(std::string("exported_aparent_power"), power123.exported_aparent_power);
			callback_parameters.Set(std::string("exported_inductive_power"), power123.exported_inductive_power);
			callback_parameters.Set(std::string("exported_capacitive_power"), power123.exported_capacitive_power);
			callback_parameters.Set(std::string("imported_active_power"), power123.imported_active_power);
			callback_parameters.Set(std::string("imported_aparent_power"), power123.imported_aparent_power);
			callback_parameters.Set(std::string("imported_inductive_power"), power123.imported_inductive_power);
			callback_parameters.Set(std::string("imported_capacitive_power"), power123.imported_capacitive_power);
			callback_parameters.Set(std::string("maximeter"), power123.maximeter);
			break;
		default:
			energy = *(EnergyParameters*)params->data;

			callback_parameters.Set(std::string("active_energy"), energy.active_energy);
			callback_parameters.Set(std::string("aparent_energy"), energy.aparent_energy);
			callback_parameters.Set(std::string("reactive_energy"), energy.inductive_energy);
			callback_parameters.Set(std::string("reactive_energy"), energy.capacitive_energy);
			callback_parameters.Set(std::string("exported_active_energy"), energy.exported_active_energy);
			callback_parameters.Set(std::string("exported_aparent_energy"), energy.exported_aparent_energy);
			callback_parameters.Set(std::string("exported_inductive_energy"), energy.exported_inductive_energy);
			callback_parameters.Set(std::string("exported_capacitive_energy"), energy.exported_capacitive_energy);
			callback_parameters.Set(std::string("imported_active_energy"), energy.imported_active_energy);
			callback_parameters.Set(std::string("imported_aparent_energy"), energy.imported_aparent_energy);
			callback_parameters.Set(std::string("imported_inductive_energy"), energy.imported_inductive_energy);
			callback_parameters.Set(std::string("imported_capacitive_energy"), energy.imported_capacitive_energy);
	}
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	jsCallback.Call({callback_parameters, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void moduleCallback(const void* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	EnergyMeter3Napi* module = (EnergyMeter3Napi*)ctx;

	// TODO wait for multiple inputs

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	// Call to the client node javascript function must be by means of a NapiThreadSafe BlockingCall
	module->callback.BlockingCall(params, threadSafeModuleCallbackWrapper);
}

Napi::Object EnergyMeter3Napi::initWrapper(Napi::Env env) {
	Napi::Function napiFunction;
	std::vector<PropertyDescriptor> classProperties;
	static Napi::FunctionReference jsClientFunctionMapper;

	classProperties.push_back(StaticMethod("checkModulePresent", &EnergyMeter3Napi::checkModulePresent));
	classProperties.push_back(InstanceMethod("configModule", &EnergyMeter3Napi::configModule));
	classProperties.push_back(InstanceMethod("resetAllEnergyMeters", &EnergyMeter3Napi::resetAllEnergyMeters));

	classProperties.push_back(InstanceMethod("getCurrentFullScale", &EnergyMeter3Napi::getCurrentFullScale));
	classProperties.push_back(InstanceMethod("getVoltageFullScale", &EnergyMeter3Napi::getVoltageFullScale));
	classProperties.push_back(InstanceMethod("getCurrentDirection", &EnergyMeter3Napi::getCurrentDirection));
	classProperties.push_back(InstanceMethod("getWorkMode", &EnergyMeter3Napi::getWorkMode));
	classProperties.push_back(InstanceMethod("getMeteringStandard", &EnergyMeter3Napi::getMeteringStandard));

	classProperties.push_back(InstanceMethod("getAllParameters", &EnergyMeter3Napi::getAllParameters));
	classProperties.push_back(InstanceMethod("getPowerParameters", &EnergyMeter3Napi::getPowerParameters));
	classProperties.push_back(InstanceMethod("getEnergyParameters", &EnergyMeter3Napi::getEnergyParameters));
	classProperties.push_back(InstanceMethod("getCombinedPowerParameters", &EnergyMeter3Napi::getCombinedPowerParameters));
	classProperties.push_back(InstanceMethod("getCombinedEnergyParameters", &EnergyMeter3Napi::getCombinedEnergyParameters));

	// DEPRECATED, to be deleted --------------------------------------------------
	classProperties.push_back(InstanceMethod("getPowerParameters_L1", &EnergyMeter3Napi::getPowerParameters_L1));
	classProperties.push_back(InstanceMethod("getPowerParameters_L2", &EnergyMeter3Napi::getPowerParameters_L2));
	classProperties.push_back(InstanceMethod("getPowerParameters_L3", &EnergyMeter3Napi::getPowerParameters_L3));
	classProperties.push_back(InstanceMethod("getPowerParameters_L123", &EnergyMeter3Napi::getPowerParameters_L123));
	classProperties.push_back(InstanceMethod("getEnergyParameters_L1", &EnergyMeter3Napi::getEnergyParameters_L1));
	classProperties.push_back(InstanceMethod("getEnergyParameters_L2", &EnergyMeter3Napi::getEnergyParameters_L2));
	classProperties.push_back(InstanceMethod("getEnergyParameters_L3", &EnergyMeter3Napi::getEnergyParameters_L3));
	classProperties.push_back(InstanceMethod("getEnergyParameters_L123", &EnergyMeter3Napi::getEnergyParameters_L123));
	//--------------------------------------------------

	classProperties.push_back(InstanceMethod("configEventAtTimeInterval", &EnergyMeter3Napi::configEventAtTimeInterval));
	classProperties.push_back(InstanceMethod("resetEventConfig", &EnergyMeter3Napi::resetEventConfig));

	classProperties.push_back(StaticValue("typeEM3", Napi::Number::New(env, ModuleType::typeEM3)));
	classProperties.push_back(StaticValue("typeEM3_250", Napi::Number::New(env, ModuleType::typeEM3_250)));

	napiFunction = DefineClass(env, "EnergyMeter3Napi", classProperties);

	jsClientFunctionMapper = Napi::Persistent(napiFunction);
	jsClientFunctionMapper.SuppressDestruct();
	return napiFunction;
}

EnergyMeter3Napi::EnergyMeter3Napi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<EnergyMeter3Napi>(info) {
	Napi::Env env(info.Env());
	EnergyMeter3ModuleCookedCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	polling_mode_ = true;
	em3_type_ = ModuleType::typeEM3;

	if (info.Length() > 0) {
		em3_type_ = static_cast<ModuleType>(info[0].As<Napi::Number>().Uint32Value());
		if (em3_type_ != 10 && em3_type_ != 14) {
			Napi::TypeError::New(env, "Error initializing EM3 module. Emeter3 type not valid").ThrowAsJavaScriptException();
		}
	}

	if (info.Length() > 1) {
		polling_mode_ = false;
		callback = Napi::ThreadSafeFunction::New(env, info[1].As<Napi::Function>(), "moduleCallback", 0, 1);
		callback_func = moduleCallback;
	}

	if (info.Length() > 2) {
		Napi::Number variant_number = info[2].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	module_ = new EnergyMeter3Module();
	if (module_->init(em3_type_, callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing EM3 module").ThrowAsJavaScriptException();
		return;
	}
	moduleInitialized = true;
}

EnergyMeter3Napi::~EnergyMeter3Napi() {
	if (!polling_mode_) callback.Abort();
	delete module_;
}

Napi::Value EnergyMeter3Napi::checkModulePresent(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "em3_type parameter must be an em3 type.").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	ModuleType em3_type = static_cast<ModuleType>(info[0].As<Napi::Number>().Uint32Value());

	if (em3_type != ModuleType::typeEM3 && em3_type != ModuleType::typeEM3_250) {
		Napi::TypeError::New(env, "unknown em3 type.").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(em3_type, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

Napi::Value EnergyMeter3Napi::configModule(const Napi::CallbackInfo& info) {
	EmodRet err_code = EmodRetOk;
	Napi::Env env = info.Env();
	Napi::Object objectJSON;
	uint32_t work_mode;
	uint32_t metering_standard;
	float full_current_scale[3];
	float full_voltage_scale[3];
	bool current_direction[3];

	objectJSON = info[0].As<Napi::Object>();

	if (moduleInitialized == false) {
		err_code = EmodRetErrModuleNotFound;
	}

	// all arguments are mandatory - they map the HTML configuration page
	if (err_code == EmodRetOk) {
		err_code = getUIntFromKey(objectJSON, "work_mode", work_mode);
	}
	if (err_code == EmodRetOk) {
		err_code = getUIntFromKey(objectJSON, "metering_standard", metering_standard);
	}
	if (err_code == EmodRetOk) {
		for (int i = 0; i < 3; i++) {
			err_code = getFloatElementFromKey(objectJSON, "current_full_scale", i, full_current_scale[i]);
			if (err_code != EmodRetOk) {
				break;
			}
		}
	}
	if (err_code == EmodRetOk) {
		for (int i = 0; i < 3; i++) {
			err_code = getFloatElementFromKey(objectJSON, "voltage_full_scale", i, full_voltage_scale[i]);
			if (err_code != EmodRetOk) {
				break;
			}
		}
	}
	if (err_code == EmodRetOk) {
		for (int i = 0; i < 3; i++) {
			err_code = getBoolElementFromKey(objectJSON, "current_direction", i, current_direction[i]);
			if (err_code != EmodRetOk) {
				break;
			}
		}
	}

	// send configuration
	if (err_code == EmodRetOk) {
		err_code = module_->configWorkMode((uint16_t)work_mode);
	}
	if (err_code == EmodRetOk) {
		err_code = module_->configMeteringStandard((uint16_t)metering_standard);
	}
	if (err_code == EmodRetOk) {
		err_code = module_->configCurrentFullScale(full_current_scale[0], full_current_scale[1], full_current_scale[2]);
	}
	if (err_code == EmodRetOk) {
		err_code = module_->configVoltageFullScale(full_voltage_scale[0], full_voltage_scale[1], full_voltage_scale[2]);
	}
	if (err_code == EmodRetOk) {
		err_code = module_->configCurrentDirection(current_direction[0], current_direction[1], current_direction[2]);
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::resetAllEnergyMeters(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	EmodRet err_code = EmodRetOk;

	if (moduleInitialized == false) {
		err_code = EmodRetErrModuleNotFound;
	}

	if (err_code == EmodRetOk) {
		err_code = module_->resetAllEnergyMeter();
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::getCurrentFullScale(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	float full_current_L1, full_current_L2, full_current_L3;

	if (this->module_->getCurrentFullScale(&full_current_L1, &full_current_L2, &full_current_L3) != 0) {
		Napi::TypeError::New(env, "Error getting current full scale").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array current_full_scale_array = Napi::Array::New(env, 3);

	current_full_scale_array[0u] = Napi::Number::New(env, full_current_L1);
	current_full_scale_array[1u] = Napi::Number::New(env, full_current_L2);
	current_full_scale_array[2u] = Napi::Number::New(env, full_current_L3);

	return current_full_scale_array;
}

Napi::Value EnergyMeter3Napi::getVoltageFullScale(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	float full_voltage_L1, full_voltage_L2, full_voltage_L3;

	if (this->module_->getVoltageFullScale(&full_voltage_L1, &full_voltage_L2, &full_voltage_L3) != 0) {
		Napi::TypeError::New(env, "Error getting voltage full scale").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array voltage_full_scale_array = Napi::Array::New(env, 3);

	voltage_full_scale_array[0u] = Napi::Number::New(env, full_voltage_L1);
	voltage_full_scale_array[1u] = Napi::Number::New(env, full_voltage_L2);
	voltage_full_scale_array[2u] = Napi::Number::New(env, full_voltage_L3);

	return voltage_full_scale_array;
}

Napi::Value EnergyMeter3Napi::getCurrentDirection(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	bool current_direction_L1, current_direction_L2, current_direction_L3;

	if (this->module_->getCurrentDirection(&current_direction_L1, &current_direction_L2, &current_direction_L3) != 0) {
		Napi::TypeError::New(env, "Error getting current direction").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array current_direction_array = Napi::Array::New(env, 3);

	current_direction_array[0u] = Napi::Boolean::New(env, current_direction_L1);
	current_direction_array[1u] = Napi::Boolean::New(env, current_direction_L2);
	current_direction_array[2u] = Napi::Boolean::New(env, current_direction_L3);

	return current_direction_array;
}

Napi::Value EnergyMeter3Napi::getWorkMode(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint16_t mode;

	if (this->module_->getWorkMode(&mode) != 0) {
		Napi::TypeError::New(env, "Error getting working mode").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, mode);
}

Napi::Value EnergyMeter3Napi::getMeteringStandard(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint16_t standard;

	if (this->module_->getMeteringStandard(&standard) != 0) {
		Napi::TypeError::New(env, "Error getting metering standard").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, standard);
}

Napi::Value EnergyMeter3Napi::getPowerParameters(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	PhasePowerParameters parameters;
	Napi::Object callback_parameters = Napi::Object::New(env);

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "phase parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	int phase = info[0].As<Napi::Number>().Int32Value();

	if (module_->getPowerParameters(phase, &parameters) != EmodRetOk) {
		Napi::TypeError::New(env, "Error getting power parameters").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	callback_parameters.Set(std::string("voltage"), parameters.voltage);
	callback_parameters.Set(std::string("current"), parameters.current);
	callback_parameters.Set(std::string("frequency"), parameters.frequency);
	callback_parameters.Set(std::string("cosine"), parameters.cosine);
	callback_parameters.Set(std::string("angle"), parameters.angle);
	callback_parameters.Set(std::string("active_power"), parameters.active_power);
	callback_parameters.Set(std::string("aparent_power"), parameters.aparent_power);
	callback_parameters.Set(std::string("reactive_power"), parameters.reactive_power);
	callback_parameters.Set(std::string("exported_active_power"), parameters.exported_active_power);
	callback_parameters.Set(std::string("exported_aparent_power"), parameters.exported_aparent_power);
	callback_parameters.Set(std::string("exported_inductive_power"), parameters.exported_inductive_power);
	callback_parameters.Set(std::string("exported_capacitive_power"), parameters.exported_capacitive_power);
	callback_parameters.Set(std::string("imported_active_power"), parameters.imported_active_power);
	callback_parameters.Set(std::string("imported_aparent_power"), parameters.imported_aparent_power);
	callback_parameters.Set(std::string("imported_inductive_power"), parameters.imported_inductive_power);
	callback_parameters.Set(std::string("imported_capacitive_power"), parameters.imported_capacitive_power);
	callback_parameters.Set(std::string("maximeter"), parameters.maximeter);

	return callback_parameters;
}

Napi::Value EnergyMeter3Napi::getEnergyParameters(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	EnergyParameters parameters;
	Napi::Object callback_parameters = Napi::Object::New(env);

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "phase parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	int phase = info[0].As<Napi::Number>().Int32Value();

	if (module_->getEnergyParameters(phase, &parameters) != EmodRetOk) {
		Napi::TypeError::New(env, "Error getting energy parameters").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	callback_parameters.Set(std::string("active_energy"), parameters.active_energy);
	callback_parameters.Set(std::string("aparent_energy"), parameters.aparent_energy);
	callback_parameters.Set(std::string("reactive_energy"), parameters.inductive_energy);
	callback_parameters.Set(std::string("reactive_energy"), parameters.capacitive_energy);
	callback_parameters.Set(std::string("exported_active_energy"), parameters.exported_active_energy);
	callback_parameters.Set(std::string("exported_aparent_energy"), parameters.exported_aparent_energy);
	callback_parameters.Set(std::string("exported_inductive_energy"), parameters.exported_inductive_energy);
	callback_parameters.Set(std::string("exported_capacitive_energy"), parameters.exported_capacitive_energy);
	callback_parameters.Set(std::string("imported_active_energy"), parameters.imported_active_energy);
	callback_parameters.Set(std::string("imported_aparent_energy"), parameters.imported_aparent_energy);
	callback_parameters.Set(std::string("imported_inductive_energy"), parameters.imported_inductive_energy);
	callback_parameters.Set(std::string("imported_capacitive_energy"), parameters.imported_capacitive_energy);

	return callback_parameters;
}

Napi::Value EnergyMeter3Napi::getCombinedPowerParameters(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	CombinedPowerParameters parameters;
	Napi::Object callback_parameters = Napi::Object::New(env);

	if (module_->getPowerParameters_L123Combined(&parameters) != EmodRetOk) {
		Napi::TypeError::New(env, "Error getting combined power parameters").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	callback_parameters.Set(std::string("neutral_current"), parameters.neutral_current);
	callback_parameters.Set(std::string("power_factor"), parameters.power_factor);
	callback_parameters.Set(std::string("neutral_angle"), parameters.neutral_angle);
	callback_parameters.Set(std::string("phase_rotation"), parameters.phase_rotation);
	callback_parameters.Set(std::string("active_power"), parameters.active_power);
	callback_parameters.Set(std::string("aparent_power"), parameters.aparent_power);
	callback_parameters.Set(std::string("reactive_power"), parameters.reactive_power);
	callback_parameters.Set(std::string("exported_active_power"), parameters.exported_active_power);
	callback_parameters.Set(std::string("exported_aparent_power"), parameters.exported_aparent_power);
	callback_parameters.Set(std::string("exported_inductive_power"), parameters.exported_inductive_power);
	callback_parameters.Set(std::string("exported_capacitive_power"), parameters.exported_capacitive_power);
	callback_parameters.Set(std::string("imported_active_power"), parameters.imported_active_power);
	callback_parameters.Set(std::string("imported_aparent_power"), parameters.imported_aparent_power);
	callback_parameters.Set(std::string("imported_inductive_power"), parameters.imported_inductive_power);
	callback_parameters.Set(std::string("imported_capacitive_power"), parameters.imported_capacitive_power);
	callback_parameters.Set(std::string("maximeter"), parameters.maximeter);

	return callback_parameters;
}

Napi::Value EnergyMeter3Napi::getCombinedEnergyParameters(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	EnergyParameters parameters;
	Napi::Object callback_parameters = Napi::Object::New(env);

	if (module_->getEnergyParameters_L123Combined(&parameters) != EmodRetOk) {
		Napi::TypeError::New(env, "Error getting combined energy parameters").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	callback_parameters.Set(std::string("active_energy"), parameters.active_energy);
	callback_parameters.Set(std::string("aparent_energy"), parameters.aparent_energy);
	callback_parameters.Set(std::string("reactive_energy"), parameters.inductive_energy);
	callback_parameters.Set(std::string("reactive_energy"), parameters.capacitive_energy);
	callback_parameters.Set(std::string("exported_active_energy"), parameters.exported_active_energy);
	callback_parameters.Set(std::string("exported_aparent_energy"), parameters.exported_aparent_energy);
	callback_parameters.Set(std::string("exported_inductive_energy"), parameters.exported_inductive_energy);
	callback_parameters.Set(std::string("exported_capacitive_energy"), parameters.exported_capacitive_energy);
	callback_parameters.Set(std::string("imported_active_energy"), parameters.imported_active_energy);
	callback_parameters.Set(std::string("imported_aparent_energy"), parameters.imported_aparent_energy);
	callback_parameters.Set(std::string("imported_inductive_energy"), parameters.imported_inductive_energy);
	callback_parameters.Set(std::string("imported_capacitive_energy"), parameters.imported_capacitive_energy);

	return callback_parameters;
}

// ❌❌ DEPRECATED, to be deleted from this line to ❌ --------------------------------------------------
Napi::Value EnergyMeter3Napi::getPowerParametersOld(const Napi::CallbackInfo& info, int phase) {
	Napi::Env env(info.Env());
	EmodRet err_code = EmodRetOk;
	PhasePowerParameters parameters;
	Napi::Object callback_parameters;
	std::string suffix;

	if (phase == 1) {
		suffix.append("_L1");
	} else if (phase == 2) {
		suffix.append("_L2");
	} else if (phase == 3) {
		suffix.append("_L3");
	}

	err_code = getParametersObject(info, 0, callback_parameters);

	if (err_code == EmodRetOk) {
		err_code = module_->getPowerParameters(phase, &parameters);
	}

	if (err_code == EmodRetOk) {
		callback_parameters.Set(std::string("voltage").append(suffix), FloatToString(parameters.voltage, 3));
		callback_parameters.Set(std::string("current").append(suffix), FloatToString(parameters.current, 3));
		callback_parameters.Set(std::string("frequency").append(suffix), FloatToString(parameters.frequency, 3));
		callback_parameters.Set(std::string("cosine").append(suffix), FloatToString(parameters.cosine, 3));
		callback_parameters.Set(std::string("angle").append(suffix), FloatToString(parameters.angle, 3));
		callback_parameters.Set(std::string("active_power").append(suffix), FloatToString(parameters.active_power, 3));
		callback_parameters.Set(std::string("aparent_power").append(suffix), FloatToString(parameters.aparent_power, 3));
		callback_parameters.Set(std::string("reactive_power").append(suffix), FloatToString(parameters.reactive_power, 3));
		callback_parameters.Set(std::string("exported_active_power").append(suffix), FloatToString(parameters.exported_active_power, 3));
		callback_parameters.Set(std::string("exported_aparent_power").append(suffix), FloatToString(parameters.exported_aparent_power, 3));
		callback_parameters.Set(std::string("exported_inductive_power").append(suffix), FloatToString(parameters.exported_inductive_power, 3));
		callback_parameters.Set(std::string("exported_capacitive_power").append(suffix), FloatToString(parameters.exported_capacitive_power, 3));
		callback_parameters.Set(std::string("imported_active_power").append(suffix), FloatToString(parameters.imported_active_power, 3));
		callback_parameters.Set(std::string("imported_aparent_power").append(suffix), FloatToString(parameters.imported_aparent_power, 3));
		callback_parameters.Set(std::string("imported_inductive_power").append(suffix), FloatToString(parameters.imported_inductive_power, 3));
		callback_parameters.Set(std::string("imported_capacitive_power").append(suffix), FloatToString(parameters.imported_capacitive_power, 3));
		callback_parameters.Set(std::string("maximeter").append(suffix), FloatToString(parameters.maximeter, 3));
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::getPowerParameters_L1(const Napi::CallbackInfo& info) { return getPowerParametersOld(info, 1); }

Napi::Value EnergyMeter3Napi::getPowerParameters_L2(const Napi::CallbackInfo& info) { return getPowerParametersOld(info, 2); }

Napi::Value EnergyMeter3Napi::getPowerParameters_L3(const Napi::CallbackInfo& info) { return getPowerParametersOld(info, 3); }

Napi::Value EnergyMeter3Napi::getPowerParameters_L123(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());
	EmodRet err_code = EmodRetOk;
	CombinedPowerParameters parameters;
	Napi::Object callback_parameters;
	std::string suffix;

	suffix.append("_L123");

	err_code = getParametersObject(info, 0, callback_parameters);

	if (err_code == EmodRetOk) {
		err_code = module_->getPowerParameters_L123Combined(&parameters);
	}
	if (err_code == EmodRetOk) {
		callback_parameters.Set(std::string("neutral_current").append(suffix), FloatToString(parameters.neutral_current, 1));
		callback_parameters.Set(std::string("power_factor").append(suffix), FloatToString(parameters.power_factor, 3));
		callback_parameters.Set(std::string("neutral_angle").append(suffix), FloatToString(parameters.neutral_angle, 1));
		callback_parameters.Set(std::string("phase_rotation").append(suffix), FloatToString(parameters.phase_rotation, 0));
		callback_parameters.Set(std::string("active_power").append(suffix), FloatToString(parameters.active_power, 3));
		callback_parameters.Set(std::string("aparent_power").append(suffix), FloatToString(parameters.aparent_power, 3));
		callback_parameters.Set(std::string("reactive_power").append(suffix), FloatToString(parameters.reactive_power, 3));
		callback_parameters.Set(std::string("exported_active_power").append(suffix), FloatToString(parameters.exported_active_power, 3));
		callback_parameters.Set(std::string("exported_aparent_power").append(suffix), FloatToString(parameters.exported_aparent_power, 3));
		callback_parameters.Set(std::string("exported_inductive_power").append(suffix), FloatToString(parameters.exported_inductive_power, 3));
		callback_parameters.Set(std::string("exported_capacitive_power").append(suffix), FloatToString(parameters.exported_capacitive_power, 3));
		callback_parameters.Set(std::string("imported_active_power").append(suffix), FloatToString(parameters.imported_active_power, 3));
		callback_parameters.Set(std::string("imported_aparent_power").append(suffix), FloatToString(parameters.imported_aparent_power, 3));
		callback_parameters.Set(std::string("imported_inductive_power").append(suffix), FloatToString(parameters.imported_inductive_power, 3));
		callback_parameters.Set(std::string("imported_capacitive_power").append(suffix), FloatToString(parameters.imported_capacitive_power, 3));
		callback_parameters.Set(std::string("maximeter").append(suffix), FloatToString(parameters.maximeter, 3));
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::getEnergyParametersOld(const Napi::CallbackInfo& info, int phase) {
	Napi::Env env(info.Env());
	EmodRet err_code = EmodRetOk;
	EnergyParameters parameters;
	Napi::Object callback_parameters;
	std::string suffix;

	if (phase == 1) {
		suffix.append("_L1");
	} else if (phase == 2) {
		suffix.append("_L2");
	} else if (phase == 3) {
		suffix.append("_L3");
	} else if (phase == 4) {
		suffix.append("_L123");
	}

	err_code = getParametersObject(info, 0, callback_parameters);

	if (err_code == EmodRetOk) {
		if (phase == 4) {
			err_code = module_->getEnergyParameters_L123Combined(&parameters);
		} else {
			err_code = module_->getEnergyParameters(phase, &parameters);
		}
	}

	if (err_code == EmodRetOk) {
		callback_parameters.Set(std::string("active_energy").append(suffix), FloatToString(parameters.active_energy, 3));
		callback_parameters.Set(std::string("aparent_energy").append(suffix), FloatToString(parameters.aparent_energy, 3));
		callback_parameters.Set(std::string("reactive_energy").append(suffix), FloatToString(parameters.inductive_energy, 3));
		callback_parameters.Set(std::string("reactive_energy").append(suffix), FloatToString(parameters.capacitive_energy, 3));
		callback_parameters.Set(std::string("exported_active_energy").append(suffix), FloatToString(parameters.exported_active_energy, 3));
		callback_parameters.Set(std::string("exported_aparent_energy").append(suffix), FloatToString(parameters.exported_aparent_energy, 3));
		callback_parameters.Set(std::string("exported_inductive_energy").append(suffix), FloatToString(parameters.exported_inductive_energy, 3));
		callback_parameters.Set(std::string("exported_capacitive_energy").append(suffix), FloatToString(parameters.exported_capacitive_energy, 3));
		callback_parameters.Set(std::string("imported_active_energy").append(suffix), FloatToString(parameters.imported_active_energy, 3));
		callback_parameters.Set(std::string("imported_aparent_energy").append(suffix), FloatToString(parameters.imported_aparent_energy, 3));
		callback_parameters.Set(std::string("imported_inductive_energy").append(suffix), FloatToString(parameters.imported_inductive_energy, 3));
		callback_parameters.Set(std::string("imported_capacitive_energy").append(suffix), FloatToString(parameters.imported_capacitive_energy, 3));
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::getEnergyParameters_L1(const Napi::CallbackInfo& info) { return getEnergyParametersOld(info, 1); }

Napi::Value EnergyMeter3Napi::getEnergyParameters_L2(const Napi::CallbackInfo& info) { return getEnergyParametersOld(info, 2); }

Napi::Value EnergyMeter3Napi::getEnergyParameters_L3(const Napi::CallbackInfo& info) { return getEnergyParametersOld(info, 3); }

Napi::Value EnergyMeter3Napi::getAllParameters(const Napi::CallbackInfo& info) {
	EmodRet err_code = EmodRetOk;
	Napi::Env env(info.Env());
	Napi::Value retValue;
	bool restoreJavaScriptException = javaScriptExceptionsActive;

	javaScriptExceptionsActive = false;
	if (err_code == EmodRetOk) {
		retValue = getPowerParameters_L1(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getPowerParameters_L2(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getPowerParameters_L3(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getPowerParameters_L123(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getEnergyParameters_L1(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getEnergyParameters_L2(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getEnergyParameters_L3(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	if (err_code == EmodRetOk) {
		retValue = getEnergyParameters_L123(info);
		err_code = retValue.ToNumber().Uint32Value();
	}
	javaScriptExceptionsActive = restoreJavaScriptException;
	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::getEnergyParameters_L123(const Napi::CallbackInfo& info) { return getEnergyParametersOld(info, 4); }
//❌. Delete from ❌❌ to this line -----------------------------------------------------------------------

Napi::Value EnergyMeter3Napi::configEventAtTimeInterval(const Napi::CallbackInfo& info) {
	Napi::Env env(info.Env());

	if (moduleInitialized == false) {
		Napi::TypeError::New(env, "Module not found").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}
	Napi::Number time_out = info[0].As<Napi::Number>();

	EmodRet err_code = module_->configEventAtTimeInterval(time_out.Uint32Value());
	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}

Napi::Value EnergyMeter3Napi::resetEventConfig(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	EmodRet err_code = EmodRetOk;

	if (moduleInitialized == false) {
		err_code = EmodRetErrModuleNotFound;
	}

	if (err_code == EmodRetOk) {
		err_code = module_->resetEventConfig();
	}

	return throwJavaScriptException(env, "EnergyMeter3Napi", __FUNCTION__, em3_type_, err_code);
}
