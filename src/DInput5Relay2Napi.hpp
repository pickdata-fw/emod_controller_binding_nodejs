#ifndef _DInput5Relay2Napi_hpp_
#define _DInput5Relay2Napi_hpp_

#include <napi.h>

#include "DInput5Relay2Module.hpp"

class DInput5Relay2Napi : public Napi::ObjectWrap<DInput5Relay2Napi> {
	DInput5Relay2Module* module;
	bool polling_mode;
	static Napi::FunctionReference constructor;

public:
	Napi::ThreadSafeFunction callback;

protected:
	static Napi::Value checkModulePresent(const Napi::CallbackInfo& info);

	// relay methods
	void configPulseWidth(const Napi::CallbackInfo& info);
	Napi::Value activate(const Napi::CallbackInfo& info);
	Napi::Value deactivate(const Napi::CallbackInfo& info);
	Napi::Value activateAll(const Napi::CallbackInfo& info);
	Napi::Value deactivateAll(const Napi::CallbackInfo& info);
	Napi::Value setStatus(const Napi::CallbackInfo& info);
	Napi::Value getRelayStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllRelayStatus(const Napi::CallbackInfo& info);

	// digital input methods
	Napi::Value getInputStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllInputStatus(const Napi::CallbackInfo& info);
	void setPulseFilterTime(const Napi::CallbackInfo& info);
	Napi::Value getPulseCount(const Napi::CallbackInfo& info);
	Napi::Value getAllPulseCount(const Napi::CallbackInfo& info);
	void resetPulseCount(const Napi::CallbackInfo& info);
	void resetAllPulseCount(const Napi::CallbackInfo& info);

	// event methods for digital inputs
	void switchToMode(const Napi::CallbackInfo& info);
	void config_event_at_time_interval(const Napi::CallbackInfo& info);
	void configEventOnNewData(const Napi::CallbackInfo& info);
	void configEventOnValueChange(const Napi::CallbackInfo& info);
	void resetEventConfig(const Napi::CallbackInfo& info);
	Napi::Value getInputsOffsetId(const Napi::CallbackInfo& info);
	Napi::Value getFunctionsOffsetId(const Napi::CallbackInfo& info);

public:
	static Napi::Object init(Napi::Env env);
	DInput5Relay2Napi(const Napi::CallbackInfo& info);
	~DInput5Relay2Napi();
};

#endif