#include "CommonNapi.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include "Module.hpp"

CommonNapi::CommonNapi() {
	javaScriptExceptionsActive = true;
	moduleInitialized = false;
}

EmodRet CommonNapi::getFloatFromKey(const Napi::Object& objectJSON, const char* key, float& float_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value value = objectJSON.Get(key);
		if (value.IsNumber()) {
			float_value = value.ToNumber().FloatValue();
			errCode = EmodRetOk;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getUIntFromKey(const Napi::Object& objectJSON, const char* key, uint32_t& uint32_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value value = objectJSON.Get(key);
		if (value.IsNumber()) {
			uint32_value = value.ToNumber().Uint32Value();
			errCode = EmodRetOk;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getUIntArrayFromKey(const Napi::Object& objectJSON, const char* key, std::vector<uint32_t>& uint32_t_values) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value array_value = objectJSON.Get(key);
		if (array_value.IsArray()) {
			errCode = EmodRetOk;
			Napi::Object array = array_value.ToObject();
			for (uint32_t i = 0; i < array.As<Napi::Array>().Length(); i++) {
				Napi::Value value = array[i];
				if (value.IsNumber()) {
					uint32_t_values.push_back(value.ToNumber().Uint32Value());
				} else {
					errCode = EmodRetWrnBadInputParam;
					break;
				}
			}
		}
	}
	return errCode;
}

EmodRet CommonNapi::getFloatElement(const Napi::Object& array_object, uint32_t i, float& float_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (array_object.IsArray()) {
		Napi::Value value = array_object[i];
		if (value.IsNumber()) {
			float_value = value.ToNumber().FloatValue();
			errCode = EmodRetOk;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getBoolElement(const Napi::Object& array_object, uint32_t i, bool& bool_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (array_object.IsArray()) {
		Napi::Value value = array_object[i];
		if (value.IsBoolean()) {
			bool_value = value.ToBoolean().Value();
			errCode = EmodRetOk;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getFloatElementFromKey(const Napi::Object& objectJSON, const char* key, int i, float& float_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value array_value = objectJSON.Get(key);
		Napi::Object array_object = array_value.ToObject();
		errCode = getFloatElement(array_object, i, float_value);
	}
	return errCode;
}

EmodRet CommonNapi::getBoolElementFromKey(const Napi::Object& objectJSON, const char* key, int i, bool& bool_value) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value array_value = objectJSON.Get(key);
		Napi::Object array_object = array_value.ToObject();
		errCode = getBoolElement(array_object, i, bool_value);
	}
	return errCode;
}

EmodRet CommonNapi::getArrayObjectFromKey(const Napi::Object& objectJSON, const char* key, Napi::Object& object_out) {
	EmodRet errCode = EmodRetWrnBadInputParam;

	if (objectJSON.Has(key)) {
		Napi::Value value = objectJSON.Get(key);
		if (value.IsArray()) {
			object_out = value.ToObject();
			errCode = EmodRetOk;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getParametersObject(const Napi::CallbackInfo& info, int arg_num, Napi::Object& object_parameter) {
	EmodRet errCode = EmodRetOk;

	if (moduleInitialized == false) {
		errCode = EmodRetErrModuleNotFound;
	}
	if (errCode == EmodRetOk) {
		if (info[arg_num].IsObject()) {
			object_parameter = info[arg_num].As<Napi::Object>();
		} else {
			errCode = EmodRetWrnBadInputParam;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getParametersNumber(const Napi::CallbackInfo& info, int arg_num, Napi::Value& number_parameter) {
	EmodRet errCode = EmodRetOk;

	if (moduleInitialized == false) {
		errCode = EmodRetErrModuleNotFound;
	}
	if (errCode == EmodRetOk) {
		if (info[arg_num].IsNumber()) {
			number_parameter = info[arg_num].As<Napi::Number>();
		} else {
			errCode = EmodRetWrnBadInputParam;
		}
	}
	return errCode;
}

EmodRet CommonNapi::getParameters(const Napi::CallbackInfo& info, int arg_num, Napi::Value& parameter) {
	EmodRet errCode = EmodRetOk;

	if (moduleInitialized == false) {
		errCode = EmodRetErrModuleNotFound;
	}
	if (errCode == EmodRetOk) {
		parameter = info[arg_num];
	}
	return errCode;
}

std::string CommonNapi::FloatToString(float float_value, int prec) {
	std::stringstream stream;
	// EDU: does this instance of setprecision do anything?
	stream << float_value << std::setprecision(prec);
	return stream.str();
}

Napi::Value CommonNapi::throwJavaScriptException(const Napi::Env& env, const char* process_prefix, const char* process, ModuleType m, EmodRet errCode) {
	errCode = EmodRetMng::getFullRetVal(m, errCode);
	if (errCode != EmodRetOk) {
		char str[100];
		sprintf(str, "%s::%s failed with error code = %d", process_prefix, process, errCode);
		if (javaScriptExceptionsActive) {
			Napi::TypeError::New(env, str).ThrowAsJavaScriptException();
			return env.Undefined();
		} else {
			std::cout << str << std::endl;
		}
	}
	return Napi::Number::New(env, errCode);
}
