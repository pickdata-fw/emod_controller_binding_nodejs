#include "digital_inputs_module.hpp"

#include <cstring>

#include "Controller.hpp"

Napi::FunctionReference digital_inputs_module::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void digital_inputs_module_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void digital_inputs_module_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	digital_inputs_module* module = (digital_inputs_module*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	module->callback.BlockingCall(params, digital_inputs_module_mainthread_callback);
}

Napi::Object digital_inputs_module::Init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "digital_inputs_module",
									  {StaticMethod("check_module_present", &digital_inputs_module::check_module_present),
									   InstanceMethod("get_status", &digital_inputs_module::get_status),
									   InstanceMethod("get_all_status", &digital_inputs_module::get_all_status),
									   InstanceMethod("set_pulse_filter_time", &digital_inputs_module::set_pulse_filter_time),
									   InstanceMethod("get_pulse_filter_time", &digital_inputs_module::get_pulse_filter_time),
									   InstanceMethod("get_all_pulse_filter_time", &digital_inputs_module::get_all_pulse_filter_time),
									   InstanceMethod("get_pulse_count", &digital_inputs_module::get_pulse_count),
									   InstanceMethod("get_all_pulse_count", &digital_inputs_module::get_all_pulse_count),
									   InstanceMethod("reset_pulse_count", &digital_inputs_module::reset_pulse_count),
									   InstanceMethod("get_pulse_width", &digital_inputs_module::get_pulse_width),
									   InstanceMethod("reset_all_pulse_count", &digital_inputs_module::reset_all_pulse_count),
									   InstanceMethod("get_all_pulse_width", &digital_inputs_module::get_all_pulse_width),
									   InstanceMethod("switch_to_mode", &digital_inputs_module::switch_to_mode),
									   InstanceMethod("config_event_at_time_interval", &digital_inputs_module::config_event_at_time_interval),
									   InstanceMethod("config_event_on_new_data", &digital_inputs_module::config_event_on_new_data),
									   InstanceMethod("config_event_on_value_change", &digital_inputs_module::config_event_on_value_change),
									   InstanceMethod("reset_event_config", &digital_inputs_module::reset_event_config),
									   InstanceMethod("getInputsOffsetId", &digital_inputs_module::getInputsOffsetId),
									   InstanceMethod("getFunctionsOffsetId", &digital_inputs_module::getFunctionsOffsetId),
									   StaticValue("input01", Napi::Number::New(env, DInput10Module::DI_INPUT01)),
									   StaticValue("input02", Napi::Number::New(env, DInput10Module::DI_INPUT02)),
									   StaticValue("input03", Napi::Number::New(env, DInput10Module::DI_INPUT03)),
									   StaticValue("input04", Napi::Number::New(env, DInput10Module::DI_INPUT04)),
									   StaticValue("input05", Napi::Number::New(env, DInput10Module::DI_INPUT05)),
									   StaticValue("input06", Napi::Number::New(env, DInput10Module::DI_INPUT06)),
									   StaticValue("input07", Napi::Number::New(env, DInput10Module::DI_INPUT07)),
									   StaticValue("input08", Napi::Number::New(env, DInput10Module::DI_INPUT08)),
									   StaticValue("input09", Napi::Number::New(env, DInput10Module::DI_INPUT09)),
									   StaticValue("input10", Napi::Number::New(env, DInput10Module::DI_INPUT10)),
									   StaticValue("all_input", Napi::Number::New(env, DInput10Module::DI_ALL_INPUT)),
									   StaticValue("mode_pulse_counter", Napi::Number::New(env, DInput10Module::MODE_PULSE_COUNTER)),
									   StaticValue("mode_width_counter", Napi::Number::New(env, DInput10Module::MODE_WIDTH_COUNTER))});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

digital_inputs_module::digital_inputs_module(const Napi::CallbackInfo& info) : Napi::ObjectWrap<digital_inputs_module>(info) {
	Napi::Env env = info.Env();
	DInput10ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "digital_inputs_module_callback", 0, 1);
		callback_func = digital_inputs_module_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new DInput10Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 10DI module").ThrowAsJavaScriptException();
		return;
	}
}

digital_inputs_module::~digital_inputs_module() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value digital_inputs_module::check_module_present(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeDI10, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

Napi::Value digital_inputs_module::get_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	uint8_t status;

	if (this->module->getStatus(input_number.Uint32Value(), &status) != 0) {
		Napi::TypeError::New(env, "Error getting digital input status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, status != 0);
}

Napi::Value digital_inputs_module::get_all_status(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t status[DInput10Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllStatus(status) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs status").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, DInput10Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput10Module::NUMBER_OF_DI_INPUTS; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] != 0);
	}

	return status_array;
}

void digital_inputs_module::set_pulse_filter_time(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (info.Length() < 2 && !info[2].IsNumber()) {
		Napi::TypeError::New(env, "ms_time parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_time = info[1].As<Napi::Number>();

	if (this->module->setPulseFilterTime(input_number.Uint32Value(), ms_time.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting digital input pulse filter time").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value digital_inputs_module::get_pulse_filter_time(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t ms_time;

	if (this->module->getPulseFilterTime(input_number.Uint32Value(), &ms_time) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse filter time").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, ms_time);
}

Napi::Value digital_inputs_module::get_all_pulse_filter_time(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t ms_times[DInput10Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllPulseFilterTime(ms_times) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs pulse filter time").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array ms_times_array = Napi::Array::New(env, DInput10Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput10Module::NUMBER_OF_DI_INPUTS; i++) {
		ms_times_array[i] = Napi::Number::New(env, ms_times[i]);
	}

	return ms_times_array;
}

Napi::Value digital_inputs_module::get_pulse_count(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t count;

	if (this->module->getPulseCount(input_number.Uint32Value(), &count) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, count);
}

Napi::Value digital_inputs_module::get_all_pulse_count(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t counts[DInput10Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllPulseCount(counts) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array counts_array = Napi::Array::New(env, DInput10Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput10Module::NUMBER_OF_DI_INPUTS; i++) {
		counts_array[i] = Napi::Number::New(env, counts[i]);
	}

	return counts_array;
}

void digital_inputs_module::reset_pulse_count(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (this->module->resetPulseCount(input_number.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

void digital_inputs_module::reset_all_pulse_count(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->resetAllPulseCount() != 0) {
		Napi::TypeError::New(env, "Error resetting digital input pulse count").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value digital_inputs_module::get_pulse_width(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();
	uint32_t width;

	if (this->module->getPulseWidth(input_number.Uint32Value(), &width) != 0) {
		Napi::TypeError::New(env, "Error getting digital input pulse width").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, width);
}

Napi::Value digital_inputs_module::get_all_pulse_width(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint32_t widths[DInput10Module::NUMBER_OF_DI_INPUTS];

	if (this->module->getAllPulseWidth(widths) != 0) {
		Napi::TypeError::New(env, "Error getting digital inputs pulse count").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array widths_array = Napi::Array::New(env, DInput10Module::NUMBER_OF_DI_INPUTS);

	for (uint32_t i = 0; i < DInput10Module::NUMBER_OF_DI_INPUTS; i++) {
		widths_array[i] = Napi::Number::New(env, widths[i]);
	}

	return widths_array;
}

void digital_inputs_module::switch_to_mode(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes exactly two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "mode parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number mode = info[1].As<Napi::Number>();

	if (this->module->switchToMode(input_number.Uint32Value(), mode.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error switching mode").ThrowAsJavaScriptException();
		return;
	}
}

void digital_inputs_module::config_event_at_time_interval(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_out = info[0].As<Napi::Number>();
	if (this->module->configEventAtTimeInterval(time_out.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void digital_inputs_module::config_event_on_new_data(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->configEventOnNewData() != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void digital_inputs_module::config_event_on_value_change(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one parameter").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, DInput10Module::DI_ALL_INPUT);
	}

	if (this->module->configEventOnValueChange(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event on new data").ThrowAsJavaScriptException();
		return;
	}
}

void digital_inputs_module::reset_event_config(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	Napi::Boolean update_module;

	if (info.Length() > 0) {
		update_module = info[0].As<Napi::Boolean>();
	} else {
		update_module = Napi::Boolean::New(env, true);
	}

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "Error resetting event config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value digital_inputs_module::getInputsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getInputsOffsetId();
	return Napi::Number::New(env, offset_id);
}

Napi::Value digital_inputs_module::getFunctionsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getFunctionsOffsetId();
	return Napi::Number::New(env, offset_id);
}
