#include <napi.h>

#include "AInput12Napi.hpp"
#include "DInput2Relay1Temp2Napi.hpp"
#include "DInput5Relay2Napi.hpp"
#include "EnergyMeter3Napi.hpp"
#include "LevelMeter5_4_VR_420Napi.hpp"
#include "PowerStatusNapi.hpp"
#include "analog_inputs_relay_module.hpp"
#include "digital_inputs_module.hpp"
#include "relay_module.hpp"

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
	exports.Set("PowerStatusNapi", PowerStatusNapi::Init(env));
	exports.Set("relay_module", relay_module::Init(env));
	exports.Set("digital_inputs_module", digital_inputs_module::Init(env));
	exports.Set("analog_inputs_relay_module", analog_inputs_relay_module::Init(env));
	exports.Set("DInput5Relay2Napi", DInput5Relay2Napi::init(env));
	exports.Set("AInput12Napi", AInput12Napi::Init(env));
	exports.Set("EnergyMeter3Napi", EnergyMeter3Napi::initWrapper(env));
	exports.Set("LevelMeter5_4_VR_420Napi", LevelMeter5_4_VR_420Napi::Init(env));
	exports.Set("DInput2Relay1Temp2Napi", DInput2Relay1Temp2Napi::Init(env));

	return exports;
}

NODE_API_MODULE(emod_controller, InitAll)
