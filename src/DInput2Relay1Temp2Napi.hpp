#ifndef _DInput2Relay1Temp2Napi_hpp_
#define _DInput2Relay1Temp2Napi_hpp_

#include <napi.h>

#include "DInput2Relay1Temp2Module.hpp"

class DInput2Relay1Temp2Napi : public Napi::ObjectWrap<DInput2Relay1Temp2Napi> {
public:
	Napi::ThreadSafeFunction callback;

	static Napi::Object Init(Napi::Env env);
	DInput2Relay1Temp2Napi(const Napi::CallbackInfo& info);
	~DInput2Relay1Temp2Napi();

protected:
	DInput2Relay1Temp2Module* module;
	bool polling_mode;
	static Napi::FunctionReference constructor;

	static Napi::Value checkModulePresent(const Napi::CallbackInfo& info);

	Napi::Value getInputsOffsetId(const Napi::CallbackInfo& info);
	Napi::Value getFunctionsOffsetId(const Napi::CallbackInfo& info);

	// digital input methods
	Napi::Value getStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllStatus(const Napi::CallbackInfo& info);
	void setPulseFilterTime(const Napi::CallbackInfo& info);
	void setAllPulseFilterTime(const Napi::CallbackInfo& info);
	Napi::Value getPulseFilterTime(const Napi::CallbackInfo& info);
	Napi::Value getAllPulseFilterTime(const Napi::CallbackInfo& info);
	Napi::Value getPulseCount(const Napi::CallbackInfo& info);
	Napi::Value getAllPulseCount(const Napi::CallbackInfo& info);
	void resetPulseCount(const Napi::CallbackInfo& info);
	void resetAllPulseCount(const Napi::CallbackInfo& info);
	Napi::Value getPulseWidth(const Napi::CallbackInfo& info);
	Napi::Value getAllPulseWidth(const Napi::CallbackInfo& info);

	// relay methods
	void configPulseWidth(const Napi::CallbackInfo& info);
	void configAllPulseWidth(const Napi::CallbackInfo& info);
	Napi::Value activate(const Napi::CallbackInfo& info);
	Napi::Value deactivate(const Napi::CallbackInfo& info);
	Napi::Value activateAll(const Napi::CallbackInfo& info);
	Napi::Value deactivateAll(const Napi::CallbackInfo& info);
	Napi::Value getRelayStatus(const Napi::CallbackInfo& info);
	Napi::Value getAllRelayStatus(const Napi::CallbackInfo& info);

	// Temperature sensor methods
	void configTempSensor(const Napi::CallbackInfo& info);
	void configAllTempSensor(const Napi::CallbackInfo& info);
	Napi::Value getTempSensorConfig(const Napi::CallbackInfo& info);
	Napi::Value getTempSensor(const Napi::CallbackInfo& info);
	Napi::Value getAllTempSensor(const Napi::CallbackInfo& info);

	// event methods for digital inputs
	void switchToModeDI(const Napi::CallbackInfo& info);
	void configEventAtTimeIntervalDI(const Napi::CallbackInfo& info);
	void configEventOnNewDataDI(const Napi::CallbackInfo& info);
	void configEventOnValueChangeDI(const Napi::CallbackInfo& info);

	// Temperature Callback methods
	void configEventAtTimeIntervalTemp(const Napi::CallbackInfo& info);
	void configEventOnValueChangeTemp(const Napi::CallbackInfo& info);
	void configEventWithinRangeTemp(const Napi::CallbackInfo& info);
	void configEventOutOfRangeTemp(const Napi::CallbackInfo& info);

	// Reset event config
	void resetEventConfig(const Napi::CallbackInfo& info);
};

#endif