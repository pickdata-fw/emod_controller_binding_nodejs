#include "LevelMeter5_4_VR_420Napi.hpp"

#include <napi.h>

#include "Controller.hpp"

Napi::FunctionReference LevelMeter5_4_VR_420Napi::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void LevelMeter5_4_VR_420Napi_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void LevelMeter5_4_VR_420Napi_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	LevelMeter5_4_VR_420Napi* module = (LevelMeter5_4_VR_420Napi*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	module->callback.BlockingCall(params, LevelMeter5_4_VR_420Napi_mainthread_callback);
}

Napi::Object LevelMeter5_4_VR_420Napi::Init(Napi::Env env) {
	Napi::Function func =
		DefineClass(env, "LevelMeter5_4_VR_420Napi",
					{
						StaticMethod("checkModulePresent", &LevelMeter5_4_VR_420Napi::checkModulePresent),
						InstanceMethod("configResistiveSensorPolarity", &LevelMeter5_4_VR_420Napi::configResistiveSensorPolarity),
						InstanceMethod("configAllResistiveSensorPolarity", &LevelMeter5_4_VR_420Napi::configAllResistiveSensorPolarity),
						InstanceMethod("configCurrentSensorPolarity", &LevelMeter5_4_VR_420Napi::configCurrentSensorPolarity),
						InstanceMethod("configAllCurrentSensorPolarity", &LevelMeter5_4_VR_420Napi::configAllCurrentSensorPolarity),
						InstanceMethod("getResistiveSensorPolarity", &LevelMeter5_4_VR_420Napi::getResistiveSensorPolarity),
						InstanceMethod("getAllResistiveSensorPolarity", &LevelMeter5_4_VR_420Napi::getAllResistiveSensorPolarity),
						InstanceMethod("getCurrentSensorPolarity", &LevelMeter5_4_VR_420Napi::getCurrentSensorPolarity),
						InstanceMethod("getAllCurrentSensorPolarity", &LevelMeter5_4_VR_420Napi::getAllCurrentSensorPolarity),
						InstanceMethod("setResistiveSensorThreshold", &LevelMeter5_4_VR_420Napi::setResistiveSensorThreshold),
						InstanceMethod("setAllResistiveSensorThreshold", &LevelMeter5_4_VR_420Napi::setAllResistiveSensorThreshold),
						InstanceMethod("setResistiveSensorSensitivity", &LevelMeter5_4_VR_420Napi::setResistiveSensorSensitivity),
						InstanceMethod("setAllResistiveSensorSensitivity", &LevelMeter5_4_VR_420Napi::setAllResistiveSensorSensitivity),
						InstanceMethod("setCurrentSensorType", &LevelMeter5_4_VR_420Napi::setCurrentSensorType),
						InstanceMethod("setAllCurrentSensorType", &LevelMeter5_4_VR_420Napi::setAllCurrentSensorType),
						InstanceMethod("getResistiveLevelStatus", &LevelMeter5_4_VR_420Napi::getResistiveLevelStatus),
						InstanceMethod("getAllResistiveLevelStatus", &LevelMeter5_4_VR_420Napi::getAllResistiveLevelStatus),
						InstanceMethod("getCurrentLevelStatus", &LevelMeter5_4_VR_420Napi::getCurrentLevelStatus),
						InstanceMethod("getAllCurrentLevelStatus", &LevelMeter5_4_VR_420Napi::getAllCurrentLevelStatus),
						InstanceMethod("getPotentiometerInput", &LevelMeter5_4_VR_420Napi::getPotentiometerInput),
						InstanceMethod("get420Input", &LevelMeter5_4_VR_420Napi::get420Input),
						InstanceMethod("configEventOnNewData", &LevelMeter5_4_VR_420Napi::configEventOnNewData),
						InstanceMethod("configEventAtTimeIntervalLevel", &LevelMeter5_4_VR_420Napi::configEventAtTimeIntervalLevel),
						InstanceMethod("configEventWithinRangePotentiometer", &LevelMeter5_4_VR_420Napi::configEventWithinRangePotentiometer),
						InstanceMethod("configEventOutOfRangePotentiometer", &LevelMeter5_4_VR_420Napi::configEventOutOfRangePotentiometer),
						InstanceMethod("configEventOnValueChangePotentiometer", &LevelMeter5_4_VR_420Napi::configEventOnValueChangePotentiometer),
						InstanceMethod("configEventAtTimeIntervalPotentiometer", &LevelMeter5_4_VR_420Napi::configEventAtTimeIntervalPotentiometer),
						InstanceMethod("configEventWithinRange420", &LevelMeter5_4_VR_420Napi::configEventWithinRange420),
						InstanceMethod("configEventOutOfRange420", &LevelMeter5_4_VR_420Napi::configEventOutOfRange420),
						InstanceMethod("configEventOnValueChange420", &LevelMeter5_4_VR_420Napi::configEventOnValueChange420),
						InstanceMethod("configEventAtTimeInterval420", &LevelMeter5_4_VR_420Napi::configEventAtTimeInterval420),
						InstanceMethod("resetEventConfig", &LevelMeter5_4_VR_420Napi::resetEventConfig),
						InstanceMethod("getFunctionsOffsetId", &LevelMeter5_4_VR_420Napi::getFunctionsOffsetId),

						StaticValue("kNumberOfResistiveSensors", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors)),
						StaticValue("kNumberOfCurrentSensors", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors)),
						StaticValue("kNumberOfPotentiometers", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kNumberOfPotentiometers)),
						StaticValue("kNumberOf420Inputs", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kNumberOf420Inputs)),

						StaticValue("kMaxRefillResistance", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kMaxRefillResistance)),
						StaticValue("kMinDetectResistance", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kMinDetectResistance)),

						StaticValue("kResistiveSensor1", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensor1)),
						StaticValue("kResistiveSensor2", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensor2)),
						StaticValue("kResistiveSensor3", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensor3)),
						StaticValue("kResistiveSensor4", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensor4)),
						StaticValue("kResistiveSensor5", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensor5)),
						StaticValue("kCurrentSensor1", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kCurrentSensor1)),
						StaticValue("kCurrentSensor2", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kCurrentSensor2)),
						StaticValue("kCurrentSensor3", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kCurrentSensor3)),
						StaticValue("kCurrentSensor4", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kCurrentSensor4)),
						StaticValue("kPotentiometer1", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kPotentiometer1)),
						StaticValue("k420Input1", Napi::Number::New(env, LevelMeter5_4_VR_420Module::k420Input1)),

						StaticValue("kResistiveSensorAll", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kResistiveSensorAll)),
						StaticValue("kCurrentSensorAll", Napi::Number::New(env, LevelMeter5_4_VR_420Module::kCurrentSensorAll)),

						StaticValue("kLevelMeterTypeNotConnected", Napi::Number::New(env, LevelMeterType::kNotConnected)),
						StaticValue("kLevelMeterTypeNamur", Napi::Number::New(env, LevelMeterType::kNamur)),
						StaticValue("kLevelMeterTypeCapacitive", Napi::Number::New(env, LevelMeterType::kCapacitive)),
						StaticValue("kLevelMeterTypePNP", Napi::Number::New(env, LevelMeterType::kPNP)),
					});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

LevelMeter5_4_VR_420Napi::LevelMeter5_4_VR_420Napi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<LevelMeter5_4_VR_420Napi>(info) {
	Napi::Env env = info.Env();
	LevelMeter5_4_VR_420Module::LevelMeter5_4_VR_420ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "LevelMeter5_4_VR_420Napi_callback", 0, 1);
		callback_func = LevelMeter5_4_VR_420Napi_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new LevelMeter5_4_VR_420Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initialazing LevelMeter5_4_VR_420Module").ThrowAsJavaScriptException();
		return;
	}
}

LevelMeter5_4_VR_420Napi::~LevelMeter5_4_VR_420Napi() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value LevelMeter5_4_VR_420Napi::checkModulePresent(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeLM5_4, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void LevelMeter5_4_VR_420Napi::configResistiveSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsBoolean()) {
		Napi::TypeError::New(env, "polarity parameter must be a boolean").ThrowAsJavaScriptException();
		return;
	}

	Napi::Boolean polarity = info[1].As<Napi::Boolean>();

	if (this->module->configResistiveSensorPolarity((LevelMeter5_4_VR_420Module::ResistiveSensorID)sensor.Uint32Value(), polarity.Value() ? 1 : 0) != 0) {
		Napi::TypeError::New(env, "configResistiveSensorPolarity error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configAllResistiveSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsBoolean()) {
		Napi::TypeError::New(env, "polarity parameter must be a boolean").ThrowAsJavaScriptException();
		return;
	}

	Napi::Boolean polarity = info[0].As<Napi::Boolean>();

	if (this->module->configAllResistiveSensorPolarity(polarity.Value() ? 1 : 0) != 0) {
		Napi::TypeError::New(env, "configAllResistiveSensorPolarity error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configCurrentSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsBoolean()) {
		Napi::TypeError::New(env, "polarity parameter must be a boolean").ThrowAsJavaScriptException();
		return;
	}

	Napi::Boolean polarity = info[1].As<Napi::Boolean>();

	if (this->module->configCurrentSensorPolarity((LevelMeter5_4_VR_420Module::CurrentSensorID)sensor.Uint32Value(), polarity.Value() ? 1 : 0) != 0) {
		Napi::TypeError::New(env, "configCurrentSensorPolarity error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configAllCurrentSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsBoolean()) {
		Napi::TypeError::New(env, "polarity parameter must be a boolean").ThrowAsJavaScriptException();
		return;
	}

	Napi::Boolean polarity = info[0].As<Napi::Boolean>();

	if (this->module->configAllCurrentSensorPolarity(polarity.Value() ? 1 : 0) != 0) {
		Napi::TypeError::New(env, "configAllCurrentSensorPolarity error").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value LevelMeter5_4_VR_420Napi::getResistiveSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor = info[0].As<Napi::Number>();
	uint8_t polarity;

	if (this->module->getResistiveSensorPolarity((LevelMeter5_4_VR_420Module::ResistiveSensorID)sensor.Uint32Value(), &polarity) != 0) {
		Napi::TypeError::New(env, "getResistiveSensorPolarity error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, polarity == 1);
}

Napi::Value LevelMeter5_4_VR_420Napi::getAllResistiveSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t polarities[LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors];

	if (this->module->getAllResistiveSensorPolarity(polarities) != 0) {
		Napi::TypeError::New(env, "getAllResistiveSensorPolarity error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array polarities_array = Napi::Array::New(env, LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors);

	for (uint32_t i = 0; i < LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors; i++) {
		polarities_array[i] = Napi::Boolean::New(env, polarities[i] == 1);
	}

	return polarities_array;
}

Napi::Value LevelMeter5_4_VR_420Napi::getCurrentSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor = info[0].As<Napi::Number>();
	uint8_t polarity;

	if (this->module->getCurrentSensorPolarity((LevelMeter5_4_VR_420Module::CurrentSensorID)sensor.Uint32Value(), &polarity) != 0) {
		Napi::TypeError::New(env, "getCurrentSensorPolarity error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, polarity == 1);
}

Napi::Value LevelMeter5_4_VR_420Napi::getAllCurrentSensorPolarity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t polarities[LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors];

	if (this->module->getAllCurrentSensorPolarity(polarities) != 0) {
		Napi::TypeError::New(env, "getAllCurrentSensorPolarity error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array polarities_array = Napi::Array::New(env, LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors);

	for (uint32_t i = 0; i < LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors; i++) {
		polarities_array[i] = Napi::Boolean::New(env, polarities[i] == 1);
	}

	return polarities_array;
}

void LevelMeter5_4_VR_420Napi::setResistiveSensorThreshold(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "detect_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number detect_resistance = info[1].As<Napi::Number>();

	if (info.Length() < 3 || !info[2].IsNumber()) {
		Napi::TypeError::New(env, "refill_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number refill_resistance = info[2].As<Napi::Number>();

	if (this->module->setResistiveSensorThreshold((LevelMeter5_4_VR_420Module::ResistiveSensorID)sensor.Uint32Value(), detect_resistance.Uint32Value(),
												  refill_resistance.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "setResistiveSensorThreshold error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::setAllResistiveSensorThreshold(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "detect_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number detect_resistance = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "refill_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number refill_resistance = info[1].As<Napi::Number>();

	if (this->module->setAllResistiveSensorThreshold(detect_resistance.Uint32Value(), refill_resistance.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "setAllResistiveSensorThreshold error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::setResistiveSensorSensitivity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "refill_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number refill_resistance = info[1].As<Napi::Number>();

	if (this->module->setResistiveSensorSensitivity((LevelMeter5_4_VR_420Module::ResistiveSensorID)sensor.Uint32Value(), refill_resistance.Uint32Value()) !=
		0) {
		Napi::TypeError::New(env, "setResistiveSensorSensitivity error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::setAllResistiveSensorSensitivity(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "refill_resistance parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number refill_resistance = info[0].As<Napi::Number>();

	if (this->module->setAllResistiveSensorSensitivity(refill_resistance.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "setAllResistiveSensorSensitivity error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::setCurrentSensorType(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number sensor = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "lm_type parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number lm_type = info[1].As<Napi::Number>();

	if (this->module->setCurrentSensorType((LevelMeter5_4_VR_420Module::CurrentSensorID)sensor.Uint32Value(), (LevelMeterType)lm_type.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "setCurrentSensorType error").ThrowAsJavaScriptException();
		return;
	}
}
void LevelMeter5_4_VR_420Napi::setAllCurrentSensorType(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "lm_type parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number lm_type = info[0].As<Napi::Number>();

	if (this->module->setAllCurrentSensorType((LevelMeterType)lm_type.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "setAllCurrentSensorType error").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value LevelMeter5_4_VR_420Napi::getResistiveLevelStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor = info[0].As<Napi::Number>();
	uint8_t status;

	if (this->module->getResistiveLevelStatus((LevelMeter5_4_VR_420Module::ResistiveSensorID)sensor.Uint32Value(), &status) != 0) {
		Napi::TypeError::New(env, "getResistiveLevelStatus error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, status == 1);
}

Napi::Value LevelMeter5_4_VR_420Napi::getAllResistiveLevelStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t status[LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors];

	if (this->module->getAllResistiveLevelStatus(status) != 0) {
		Napi::TypeError::New(env, "getAllResistiveLevelStatus error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors);

	for (uint32_t i = 0; i < LevelMeter5_4_VR_420Module::kNumberOfResistiveSensors; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] == 1);
	}

	return status_array;
}

Napi::Value LevelMeter5_4_VR_420Napi::getCurrentLevelStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 || !info[0].IsNumber()) {
		Napi::TypeError::New(env, "sensor parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number sensor = info[0].As<Napi::Number>();
	uint8_t status;

	if (this->module->getCurrentLevelStatus((LevelMeter5_4_VR_420Module::CurrentSensorID)sensor.Uint32Value(), &status) != 0) {
		Napi::TypeError::New(env, "getCurrentLevelStatus error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Boolean::New(env, status == 1);
}

Napi::Value LevelMeter5_4_VR_420Napi::getAllCurrentLevelStatus(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint8_t status[LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors];

	if (this->module->getAllCurrentLevelStatus(status) != 0) {
		Napi::TypeError::New(env, "getAllCurrentLevelStatus error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array status_array = Napi::Array::New(env, LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors);

	for (uint32_t i = 0; i < LevelMeter5_4_VR_420Module::kNumberOfCurrentSensors; i++) {
		status_array[i] = Napi::Boolean::New(env, status[i] == 1);
	}

	return status_array;
}

Napi::Value LevelMeter5_4_VR_420Napi::getPotentiometerInput(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint16_t data;

	if (this->module->getPotentiometerInput(&data) != 0) {
		Napi::TypeError::New(env, "getPotentiometerInput error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, data);
}

Napi::Value LevelMeter5_4_VR_420Napi::get420Input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint16_t data;

	if (this->module->get420Input(&data) != 0) {
		Napi::TypeError::New(env, "get420Input error").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, data);
}

void LevelMeter5_4_VR_420Napi::configEventOnNewData(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->configEventOnNewData() != 0) {
		Napi::TypeError::New(env, "configEventOnNewData error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventAtTimeIntervalLevel(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_interval parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_interval = info[0].As<Napi::Number>();

	if (this->module->configEventAtTimeIntervalLevel(time_interval.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventAtTimeIntervalLevel error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventWithinRangePotentiometer(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	if (this->module->configEventWithinRangePotentiometer(low_limit.Uint32Value(), high_limit.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventWithinRangePotentiometer error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventOutOfRangePotentiometer(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	if (this->module->configEventOutOfRangePotentiometer(low_limit.Uint32Value(), high_limit.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventOutOfRangePotentiometer error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventOnValueChangePotentiometer(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "threshold parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	if (this->module->configEventOnValueChangePotentiometer(threshold.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventOnValueChangePotentiometer error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventAtTimeIntervalPotentiometer(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_interval parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_interval = info[0].As<Napi::Number>();

	if (this->module->configEventAtTimeIntervalPotentiometer(time_interval.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventAtTimeIntervalPotentiometer error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventWithinRange420(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	if (this->module->configEventWithinRange420(low_limit.Uint32Value(), high_limit.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventWithinRange420 error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventOutOfRange420(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (info.Length() < 2 || !info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	if (this->module->configEventOutOfRange420(low_limit.Uint32Value(), high_limit.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventOutOfRange420 error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventOnValueChange420(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "threshold parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	if (this->module->configEventOnValueChange420(threshold.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventOnValueChange420 error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::configEventAtTimeInterval420(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_interval parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_interval = info[0].As<Napi::Number>();

	if (this->module->configEventAtTimeInterval420(time_interval.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "configEventAtTimeInterval420 error").ThrowAsJavaScriptException();
		return;
	}
}

void LevelMeter5_4_VR_420Napi::resetEventConfig(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "resetEventConfig error").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value LevelMeter5_4_VR_420Napi::getFunctionsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getFunctionsOffsetId();
	return Napi::Number::New(env, offset_id);
}