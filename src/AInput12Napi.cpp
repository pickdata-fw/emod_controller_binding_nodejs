#include "AInput12Napi.hpp"

#include <cstring>

#include "Controller.hpp"

Napi::FunctionReference AInput12Napi::constructor;

struct callback_params {
	uint8_t* data;
	uint16_t datalen;
	uint8_t idFunction;
};

static void AInput12Module_mainthread_callback(Napi::Env env, Napi::Function jsCallback, callback_params* params) {
	Napi::Buffer<uint8_t> dataBuffer = Napi::Buffer<uint8_t>::Copy(env, params->data, params->datalen);
	Napi::Number idFunctionNumber = Napi::Number::New(env, params->idFunction);

	jsCallback.Call({dataBuffer, idFunctionNumber});

	if (params != nullptr) delete params->data;
	delete params;
}

static void AInput12Napi_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
	AInput12Napi* module = (AInput12Napi*)ctx;

	callback_params* params = new callback_params();

	params->idFunction = idFunction;
	params->datalen = datalen;
	if (datalen > 0 && data != nullptr) {
		params->data = new uint8_t[datalen];
		std::memcpy(params->data, data, datalen);
	} else {
		params->data = nullptr;
	}

	module->callback.BlockingCall(params, AInput12Module_mainthread_callback);
}

Napi::Object AInput12Napi::Init(Napi::Env env) {
	Napi::Function func = DefineClass(env, "AInput12Napi",
									  {StaticMethod("check_module_present", &AInput12Napi::check_module_present),
									   InstanceMethod("config_sample_rate", &AInput12Napi::config_sample_rate),
									   InstanceMethod("config_input", &AInput12Napi::config_input),
									   InstanceMethod("get_input_config", &AInput12Napi::get_input_config),
									   InstanceMethod("get_analog_input", &AInput12Napi::get_analog_input),
									   InstanceMethod("get_all_analog_input", &AInput12Napi::get_all_analog_input),
									   InstanceMethod("config_event_at_time_interval", &AInput12Napi::config_event_at_time_interval),
									   InstanceMethod("config_event_on_value_change", &AInput12Napi::config_event_on_value_change),
									   InstanceMethod("config_event_within_range", &AInput12Napi::config_event_within_range),
									   InstanceMethod("config_event_out_of_range", &AInput12Napi::config_event_out_of_range),
									   InstanceMethod("reset_event_config", &AInput12Napi::reset_event_config),
									   InstanceMethod("voltage_to_samples", &AInput12Napi::voltage_to_samples),
									   InstanceMethod("current_to_samples", &AInput12Napi::current_to_samples),
									   InstanceMethod("samples_to_voltage", &AInput12Napi::samples_to_voltage),
									   InstanceMethod("samples_to_current", &AInput12Napi::samples_to_current),
									   InstanceMethod("getInputsOffsetId", &AInput12Napi::getInputsOffsetId),
									   InstanceMethod("getNumAnalogInputs", &AInput12Napi::getNumAnalogInputs),
									   StaticValue("input01", Napi::Number::New(env, AInput12Module::AI_INPUT01)),
									   StaticValue("input02", Napi::Number::New(env, AInput12Module::AI_INPUT02)),
									   StaticValue("input03", Napi::Number::New(env, AInput12Module::AI_INPUT03)),
									   StaticValue("input04", Napi::Number::New(env, AInput12Module::AI_INPUT04)),
									   StaticValue("input05", Napi::Number::New(env, AInput12Module::AI_INPUT05)),
									   StaticValue("input06", Napi::Number::New(env, AInput12Module::AI_INPUT06)),
									   StaticValue("input07", Napi::Number::New(env, AInput12Module::AI_INPUT07)),
									   StaticValue("input08", Napi::Number::New(env, AInput12Module::AI_INPUT08)),
									   StaticValue("input09", Napi::Number::New(env, AInput12Module::AI_INPUT09)),
									   StaticValue("input10", Napi::Number::New(env, AInput12Module::AI_INPUT10)),
									   StaticValue("input11", Napi::Number::New(env, AInput12Module::AI_INPUT11)),
									   StaticValue("input12", Napi::Number::New(env, AInput12Module::AI_INPUT12)),
									   StaticValue("all_input", Napi::Number::New(env, AInput12Module::AI_ALL_INPUT)),
									   StaticValue("max_inputs", Napi::Number::New(env, AInput12Module::NUMBER_OF_AI_INPUTS))});

	constructor = Napi::Persistent(func);
	constructor.SuppressDestruct();

	return func;
}

AInput12Napi::AInput12Napi(const Napi::CallbackInfo& info) : Napi::ObjectWrap<AInput12Napi>(info) {
	Napi::Env env = info.Env();
	AInput12ModuleCallback_Type callback_func = nullptr;
	uint8_t variant = 1;
	this->polling_mode = true;

	if (info.Length() > 0) {
		this->polling_mode = false;
		this->callback = Napi::ThreadSafeFunction::New(env, info[0].As<Napi::Function>(), "AInput12Napi_callback", 0, 1);
		callback_func = AInput12Napi_callback;
	}

	if (info.Length() > 1) {
		Napi::Number variant_number = info[1].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	this->module = new AInput12Module();
	if (this->module->init(callback_func, this, variant) != 0) {
		Napi::TypeError::New(env, "Error initializing 12AI module").ThrowAsJavaScriptException();
		return;
	}
}

AInput12Napi::~AInput12Napi() {
	if (!this->polling_mode) this->callback.Abort();
	delete this->module;
}

Napi::Value AInput12Napi::check_module_present(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	uint8_t variant = 1;
	bool module_present = false;

	if (info.Length() > 0) {
		Napi::Number variant_number = info[0].As<Napi::Number>();
		variant = variant_number.Uint32Value();
	}

	Controller::isModulePresent(ModuleType::typeAI12, variant, &module_present);

	return Napi::Boolean::New(env, module_present);
}

void AInput12Napi::config_sample_rate(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "ms_period parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number ms_period = info[0].As<Napi::Number>();

	if (this->module->configSampleRate(ms_period.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting analog inputs sample rate").ThrowAsJavaScriptException();
		return;
	}
}

void AInput12Napi::config_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "voltage_input_mask parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number voltage_input_mask = info[0].As<Napi::Number>();

	if (this->module->configInput(voltage_input_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting analog input config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value AInput12Napi::get_input_config(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	uint16_t config;

	if (this->module->getInputConfig(&config) != 0) {
		Napi::TypeError::New(env, "Error getting analog input config").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, config);
}

Napi::Value AInput12Napi::get_analog_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1 && !info[0].IsNumber()) {
		Napi::TypeError::New(env, "input_number parameter must be a number").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number input_number = info[0].As<Napi::Number>();

	float data;

	if (this->module->getAnalogInput(input_number.Uint32Value(), &data) != 0) {
		Napi::TypeError::New(env, "Error getting analog input value").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	return Napi::Number::New(env, data);
}

Napi::Value AInput12Napi::get_all_analog_input(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	float data[AInput12Module::NUMBER_OF_AI_INPUTS];

	if (this->module->getAllAnalogInput(data) != 0) {
		Napi::TypeError::New(env, "Error getting analog inputs value").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Array data_array = Napi::Array::New(env, AInput12Module::NUMBER_OF_AI_INPUTS);

	for (uint32_t i = 0; i < AInput12Module::NUMBER_OF_AI_INPUTS; i++) {
		data_array[i] = Napi::Number::New(env, data[i]);
	}

	return data_array;
}

void AInput12Napi::config_event_at_time_interval(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "time_out parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number time_out = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput12Module::AI_ALL_INPUT);
	}

	if (this->module->configEventAtTimeInterval(time_out.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void AInput12Napi::config_event_on_value_change(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes at least one argument").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "threshold parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number threshold = info[0].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 1 && info[1].IsNumber()) {
		event_mask = info[1].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput12Module::AI_ALL_INPUT);
	}

	if (this->module->configEventOnValueChange(threshold.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void AInput12Napi::config_event_within_range(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput12Module::AI_ALL_INPUT);
	}

	if (this->module->configEventWithinRange(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void AInput12Napi::config_event_out_of_range(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::TypeError::New(env, "Method takes at least two arguments").ThrowAsJavaScriptException();
		return;
	}

	if (!info[0].IsNumber()) {
		Napi::TypeError::New(env, "low_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number low_limit = info[0].As<Napi::Number>();

	if (!info[1].IsNumber()) {
		Napi::TypeError::New(env, "high_limit parameter must be a number").ThrowAsJavaScriptException();
		return;
	}

	Napi::Number high_limit = info[1].As<Napi::Number>();

	Napi::Number event_mask;

	if (info.Length() > 2 && info[2].IsNumber()) {
		event_mask = info[2].As<Napi::Number>();
	} else {
		event_mask = Napi::Number::New(env, AInput12Module::AI_ALL_INPUT);
	}

	if (this->module->configEventOutOfRange(low_limit.Uint32Value(), high_limit.Uint32Value(), event_mask.Uint32Value()) != 0) {
		Napi::TypeError::New(env, "Error setting event at time interval").ThrowAsJavaScriptException();
		return;
	}
}

void AInput12Napi::reset_event_config(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	Napi::Boolean update_module;

	if (info.Length() > 0) {
		update_module = info[0].As<Napi::Boolean>();
	} else {
		update_module = Napi::Boolean::New(env, true);
	}

	if (this->module->resetEventConfig() != 0) {
		Napi::TypeError::New(env, "Error resetting event config").ThrowAsJavaScriptException();
		return;
	}
}

Napi::Value AInput12Napi::voltage_to_samples(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number voltage = info[0].As<Napi::Number>();

	uint16_t samples = module->voltageToSamples(voltage.FloatValue());

	return Napi::Number::New(env, samples);
}

Napi::Value AInput12Napi::current_to_samples(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number current = info[0].As<Napi::Number>();

	uint16_t samples = module->currentToSamples(current.FloatValue());

	return Napi::Number::New(env, samples);
}

Napi::Value AInput12Napi::samples_to_voltage(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number samples = info[0].As<Napi::Number>();

	float voltage = module->samplesToVoltage(samples.Uint32Value());

	return Napi::Number::New(env, voltage);
}

Napi::Value AInput12Napi::samples_to_current(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();

	if (info.Length() < 1) {
		Napi::TypeError::New(env, "Method takes exactly one argument").ThrowAsJavaScriptException();
		return env.Undefined();
	}

	Napi::Number samples = info[0].As<Napi::Number>();

	float current = module->samplesToCurrent(samples.Uint32Value());

	return Napi::Number::New(env, current);
}

Napi::Value AInput12Napi::getInputsOffsetId(const Napi::CallbackInfo& info) {
	uint8_t offset_id;
	Napi::Env env = info.Env();

	offset_id = module->getInputsOffsetId();
	return Napi::Number::New(env, offset_id);
}

Napi::Value AInput12Napi::getNumAnalogInputs(const Napi::CallbackInfo& info) {
	uint8_t num_inputs;
	Napi::Env env = info.Env();

	num_inputs = module->getNumAnalogInputs();
	return Napi::Number::New(env, num_inputs);
}
