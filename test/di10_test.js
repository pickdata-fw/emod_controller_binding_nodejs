console.log("Starting node api binding test for di10 ...");

var event_mode = 0;
var variant_2_present = 1;

const controller = require('bindings')('emod_controller');

if (controller.digital_inputs_module.check_module_present(1) ){
	console.log("Variant 1 is present\n");
}
else{
	console.log("Error: Variant 1 is not present");
	throw new Error ("Error: Variant 1 is not present");
}

var v1 = new controller.digital_inputs_module(function (data, idFunction){
		var ts = Date.now() / 1000;
		if(idFunction == 1){
			var inputs_status = data.readUInt16BE(0);
			console.log(ts.toString() + ": callback v1 idFunction = " + idFunction.toString() + " Status = " + inputs_status.toString(16));
		}
		else{
			var pulse_count = data.readUInt32BE(0);
			console.log(ts.toString() + ": callback v1 idFunction = " + idFunction.toString() + " Count = " + pulse_count.toString());
		}
	}, 1);

console.log("Reset event config for variant 1 ... ")
try{
	v1.reset_event_config(true);
}
catch (e) {
	console.log("\tError");
	console.log(e);
	throw new Error ("Error: Reset event config v1");
}
console.log("\tSuccess\n");

for (var i = 0; i < 10; i++) {
	console.log("Set Filter Time Variant 1, Input " + (i + 1).toString() + " ...")
	try{
		v1.set_pulse_filter_time(1 << i, 7);
	}
	catch (e) {
		console.log("\tError");
		console.log(e);
		throw new Error ("Error Set Filter Time Variant 1, Input " + (i + 1).toString());
	}
	console.log("\tSuccess\n");	
}

if(event_mode == 0){
	console.log("Config event on new data for variant 1 ... ")
	try{
		v1.config_event_on_new_data();
	}
	catch (e) {
		console.log("\tError");
		console.log(e);
		throw new Error ("Error: Config event on new data v1");
	}
	console.log("\tSuccess\n");
}
else{
	console.log("Switch mode to Pulse Count for variant 1 ... ")
	try{
		v1.switch_to_mode(controller.digital_inputs_module.all_input, controller.digital_inputs_module.mode_pulse_counter);
	}
	catch (e) {
		console.log("\tError");
		console.log(e);
		throw new Error ("Error: Switch mode to Pulse Count v1");
	}
	console.log("\tSuccess\n");

	console.log("Config event on pulse count for variant 1 ... ")
	try{
		v1.config_event_on_value_change(1);
	}
	catch (e) {
		console.log("\tError");
		console.log(e);
		throw new Error ("Error: Config event on pulse count v1");
	}
	console.log("\tSuccess\n");
}

if(variant_2_present == 1){

	if (controller.digital_inputs_module.check_module_present(2) ){
		console.log("Variant 2 is present\n");
	}
	else{
		console.log("Error: Variant 2 is not present");
		throw new Error ("Error: Variant 2 is not present");
	}

	var v2 = new controller.digital_inputs_module(function (data, idFunction){
			var ts = Date.now() / 1000;
			if(idFunction == 1){
				var inputs_status = data.readUInt16BE(0);
				console.log(ts.toString() + ": callback v2 idFunction = " + idFunction.toString() + " Status = " + inputs_status.toString(16));
			}
			else{
				var pulse_count = data.readUInt32BE(0);
				console.log(ts.toString() + ": callback v2 idFunction = " + idFunction.toString() + " Count = " + pulse_count.toString());
			}
		}, 2);

	console.log("Reset event config for variant 2 ... ")
	try{
		v2.reset_event_config(true);
	}
	catch (e) {
		console.log("\tError");
		console.log(e);
		throw new Error ("Error: Reset event config v2");
	}
	console.log("\tSuccess\n");

	for (var i = 0; i < 10; i++) {
		console.log("Set Filter Time Variant 2, Input " + (i + 1).toString() + " ...")
		try{
			v2.set_pulse_filter_time(1 << i, 7);
		}
		catch (e) {
			console.log("\tError");
			console.log(e);
			throw new Error ("Error Set Filter Time Variant 2, Input " + (i + 1).toString());
		}
		console.log("\tSuccess\n");	
	}

	if(event_mode == 0){
		console.log("Config event on new data for variant 2 ... ")
		try{
			v2.config_event_on_new_data();
		}
		catch (e) {
			console.log("\tError");
			console.log(e);
			throw new Error ("Error: Config event on new data v2");
		}
		console.log("\tSuccess\n");
	}
	else{
		console.log("Switch mode to Pulse Count for variant 2 ... ")
		try{
			v2.switch_to_mode(controller.digital_inputs_module.all_input, controller.digital_inputs_module.mode_pulse_counter);
		}
		catch (e) {
			console.log("\tError");
			console.log(e);
			throw new Error ("Error: Switch mode to Pulse Count v2");
		}
		console.log("\tSuccess\n");

		console.log("Config event on pulse count for variant 2 ... ")
		try{
			v2.config_event_on_value_change(1);
		}
		catch (e) {
			console.log("\tError");
			console.log(e);
			throw new Error ("Error: Config event on pulse count v2");
		}
		console.log("\tSuccess\n");
	}
}
