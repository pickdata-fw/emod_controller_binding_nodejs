//EDU: using async and await to define delay (to emulate sleep in JS). It might be wrongly used. 

(async () => {
    const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
    await delay(1000) /// use for waiting 1 second.

    const controller = require('bindings')('emod_controller');

    console.log("Level Meter module is connected:", controller.LevelMeter5_4_VR_420Napi.checkModulePresent())
    // Level Meter module is connected: true

    var lm5_4 = new controller.LevelMeter5_4_VR_420Napi;

    var i = 0;

    //checking leds this should light green leds
    for (j = 0; j < 5; j++) {
        lm5_4.configAllResistiveSensorPolarity(true);
        await delay(500);
        i++;
        console.log("test i = " + i + "\n")
        lm5_4.configAllResistiveSensorPolarity(false);
        await delay(500);
        i++;
        console.log("test i = " + i + "\n")
    }

    lm5_4.setAllCurrentSensorType(2)
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.configCurrentSensorPolarity(15, true);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.configAllCurrentSensorPolarity(false);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.configAllResistiveSensorPolarity(true);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.configAllResistiveSensorPolarity(false);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    console.log(lm5_4.getResistiveSensorPolarity(1 << 0));
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")
    // false


    console.log(lm5_4.getAllResistiveSensorPolarity());
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")
    // [ false, false, false, false, false ]

    lm5_4.configCurrentSensorPolarity(1 << 3, true)
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    console.log(lm5_4.getCurrentSensorPolarity(1 << 3) == true)
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")
    // true

    console.log(lm5_4.getAllCurrentSensorPolarity());
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")
    // [ false, false, false, true ]


    lm5_4.setResistiveSensorSensitivity(8, 34);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.setAllResistiveSensorSensitivity(34);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.setCurrentSensorType(4, 0);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    lm5_4.setAllCurrentSensorType(2);
    //await delay(500);
    i++;
    console.log("test i = " + i + "\n")

    console.log(lm5_4.getResistiveLevelStatus(4));

    console.log(lm5_4.getAllResistiveLevelStatus());

    console.log(lm5_4.getCurrentLevelStatus(2));

    console.log(lm5_4.getAllCurrentLevelStatus());
    console.log(lm5_4.getPotentiometerInput());
    console.log(lm5_4.get420Input());
})()