{
	"targets": [
		{
			"target_name": "emod_controller",
			"cflags!": [ "-fno-exceptions" ],
			"cflags_cc!": [ "-fno-exceptions" ],
			"sources": [
				"./src/CommonNapi.cpp",
				"./src/emod_controller.cpp",
				"./src/PowerStatusNapi.cpp",
				"./src/relay_module.cpp",
				"./src/digital_inputs_module.cpp",
				"./src/analog_inputs_relay_module.cpp",
				"./src/analog_inputs_relay_module.cpp",
				"./src/DInput5Relay2Napi.cpp",
				"./src/AInput12Napi.cpp",
				"./src/EnergyMeter3Napi.cpp",
				"./src/LevelMeter5_4_VR_420Napi.cpp",
				"./src/DInput2Relay1Temp2Napi.cpp",
			],
			"include_dirs": [
				"<!@(node -p \"require('node-addon-api').include\")",
				"../emod_controller/build/install/include",
				"../emod_controller/build/install/include/emod_private"
			],
			'link_settings': {
				'libraries': [
						'-L../../emod_controller/build/install/lib',
						'-Wl,-rpath-link,../../emod_controller/build/install/lib',
						'-lcontroller',
						'-lpwr',
						'-lrelay8_module',
						'-ldinput10_module',
						'-lai7r2_module',
						'-ldi5r2_module',
						'-lai12_module',
						'-lemeter3_module',
						'-llmeter5_4_VR_420_module',
						'-lhal',
						'-ldi2r1t2_module',
				],
			},
			'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ],
		}
	]
}